>[!Question] Comment refactoriser sans casser les tests?
>>[!Define] Refactoring additif
>>Créer une nouvelle fonction quand on fait son refactoring, et utiliser l'ancienne et la nouvelle jusqu'à ce que l'on ait plus besoin de l'ancienne.

>[!Question] Fonctionnement / performance
>D'un point de vue business, le fonctionnement du code est prioritaire.
>
>>[!Important]
>>Il vaux mieux aller vite à une solution qui marche pour satisfaire le business, quitte à le retravailler/améliorer après, plutôt que de passer beaucoup de temps sur un petit bout de code.
>
>Les contraintes de performance n'existent que si elles sont exprimées par le business.

# Ordre des transformations
>[!Info] Approche de Robert C. Martin
>https://en.wikipedia.org/wiki/Transformation_Priority_Premise

>[!Quote]
>1. ({} → nil) no code at all → code that employs nil
>2. (nil → constant)
>3. (constant → constant+) a simple constant to a more complex constant
>4. (constant → scalar) replacing a constant with a variable or an argument
>5. (statement → statements) adding more unconditional statements.
>6. (unconditional → if) splitting the execution path
>7. (scalar → array)
>8. (array → container)
>9. (statement → tail-recursion)
>10. (if → while)
>11. (statement → non-tail-recursion)
>12. (expression → function) replacing an expression with a function or algorithm
>13. (variable → assignment) replacing the value of a variable.
>14. (case) adding a case (or else) to an existing switch or if

>[!Define] AAA
>- Arrange: préparer l'environnement,
>- Act: exécuter la fonction,
>- Assert: vérifier le retour.

# Variantes / Approfondissements
- ATDD,
- BDD,
- Double loop,
- PBT,
- Mutation testing,
- Pair Programming,
- Mob / Ensemble Programming,
- TCR,
- Intégration continu,
- SOLID,
- Facile à utiliser, difficile à mal utiliser,
- Forte cohésion / Faible couplage.