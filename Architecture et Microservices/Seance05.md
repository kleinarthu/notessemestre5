# Travail de groupe
- 3 minimum,
- rendu sur github.
# Kafka

## Historique
- Sert à streamer des events, faire du calcul distribué, et monter en charge assez rapidement.
- Créé en 2011,
- Créé par Linkedin,
- Maintenu par Apache.

## Architecture
>[!Define] Architecture en bus de message
>Un système encaisse les messages et réagit en fonction.
>On a un but et toutes les applications peuvent écrire ou lire dessus.

>[!Define] Cluster Kafka
>Ensemble de Brocker qui fonctionne ensemble.

>[!Define] Brocker
>Instance de Kafka
>Machine située à un autre endroit du globe.
>Contient un ou plusieurs topic et plusieurs partitions.
>Peut avoir des replicas.

>[!Define] Topic
>Type de message.
>Permet de regrouper des messages.

>[!Define] Partition
>Partie d'un topic immuable, ordonnée, et FIFO.
>Permet la parallélisation.

>[!Define] Producer
>Système qui publie dans un topic Kafka.

>[!Example]
>``` python
>from kafka import KafkaProducer
>producer = KafkaProducer(bootstrap_servers = 'localhost:1234')
>producer.send('topic', b'message')
>```

>[!Define] Consumer
>Système qui lit dans un topic Kafka.
>La lecture n'est pas destructrice.

>[!Example]
>``` python
>from kafka import KafkaConsumer
>consumer = KafkaConsumer('topic')
>for msg in consumer:
>	print(msg)
>```

>[!Define] Message
>Chaîne de caractères.
>Souvent en JSON.