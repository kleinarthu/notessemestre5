# Data Science et images satellitaires
## Télédétection optique et radar
>[!Define] Télédétection  
>Techniques de détection satellitaire à distance à partir des rayonnements.

### Types de télédétection
>[!Define] Télédétection passive et images optiques  
>Ondes émises par le Soleil puis réfléchies par la surface de la Terre.

>[!Define] Télédétection actives et images radars  
>Ondes émises par un émetteur artificiel placé sur le satellite puis réfléchies par la surface de la terre.

>[!Success] Avantages  
>Images optiques intuitive

>[!Failure] Inconvénients  
>Pas d'image optique la nuit, et selon les conditions météos.

### Fonctionnement
Lorsqu'elle reçoit un rayonnement, la cible va:
- Absorber une partie,
- Transmettre une partie aux surfaces sous-jacentes,
- Réfléchir une partie.

>[!Important] Réflectance  
>${énergie\_réfléchie} \over {énergie\_incidente}$

Cette réflectance dépend de la longueur d'onde, et différentes longueurs d'ondes permettent différentes études.

On peut tracer une signature spectrale sous forme de courbe de la réflectance d'un matériau fonction de sa longueur d'onde.

>[!Define] Indice de végétation (NDVI)  
>$NDVI = {{NIR - RED} \over {NIR + RED}}$
>où:
>- $NIR$: Réflectance des canaux proches infrarouges,
>- $RED$: Réflectance des canaux rouges.

D'autres indices existent pour d'autres surfaces (eg eau).

### Satellites disponibles
- Sentinel (1 ou pour les radars, 2 et 3 pour optiques): satellite européen qui produit des images de hautes résolutions (10m de précision),
- Landsat: programme de la NASA, initialement prévu pour les sols agricoles,
- MODIS: satellite de la NASA pour les données météorologiques,
- Pléiades: satellite beaucoup plus précis et rapides, par Aibus mais beaucoup plus chères.

### Utilisation
[API Sentinel Hub Process (python)](https://sentinelhub-py.readthedocs.io/en/latest/)

### Domaine d'application
Gestion des ressources agricoles et répartition des parcelles agricoles.

### Format de données
- Raster: Par "pixels",
- Vecteur: Par polygones.

# Preprocessing
## Traiter les valeurs nulles
- Interpolation,
- KNN (K nearest neighbors),
- inpainting (pour les images, par exemple TELEA et Navier-Strokes).

## Segmentation
>[!Define] Segmentation d'image  
>Diviser ou partitionner une image pour regrouper des pixels selon les propriétés des régions.

1. Pre-processing,
2. Clustering.
