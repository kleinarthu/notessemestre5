# Services
```ts
ng g service shared/services/auth
```

>[!Define] Service
>Sert à partager du code (injectable).
# Technologies multi-threads et mono-threads
>[!Example]
PHP alloue un thread à chaque utilisateur.
Javascript ne fait qu'une seule opération à la fois.

>[!Failure] Inconvénient
>Délai sur la gestion des événements (clics, ...)

D'où l'importance des requêtes XHR asynchrones.

>[!Define] XHR
>**XML HTTP Request**: Requête depuis le navigateur vers une serveur distant (seul solution pour récupérer des données avec javascript).

Visibles dans Firefox -> Sidepannel -> Réseau -> Fetch/XHR

Angular ré-utilise la gestion de javascript de base et rajoute de la sécurité.

```ts
this.http.get<Array<EvenementI>>('assets/data/evenements.json').subscribe();
```

# Pipes personnalisés
```shell
ng g pipe shared/pipe/events
```
# Sécurité
**3 niveaux**
# Niveau 1
Lié à l'organisation de l'application.

>[!Hint]
>Angular prend en charge la minification.
>``` bash
># Crée les fichiers qui seront mis à disposition des utilisateurs
>ng build
>```

Tout le code écrit est potentiellement accessible (car javascript).
*Solution*: Créer des modules.
```shell
ng g module organisation --routing
```