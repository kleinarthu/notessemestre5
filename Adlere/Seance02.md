# Gitlab CI
>[!Define] Gitlab CI
>Équivalent Gitlab de github actions.

Fichier: .gitlab-ci.yml

>[!Define] Runner
>Composant qui permet d'exécuter les jobs définis dans les pipelines de GitLab.

>[!Define] Runner partagé
> Runner commun à l'ensemble du gitlab.

>[!Define] Runner spécifique
>Runner associé à un projet.

On peut définir un grand nombre de variables d'environnements sur gitlab (globales créée par admin, automatiques (commits, utilisateurs), de groupe, spécifiques au projet, ...)