# Virtualization with KVM (Kernel Virtual Machine)
Hardware Virtualization
## Verify that the VT-x virtualization is enabled
``` shell
grep --color vmx /cpu/info
```

## Installation
``` shell
sudo apt install qemu-kvm
sudo apt install virt-manager
```

## Supported image formats
- Qcow2,
- Raw.

```ssh
qemu-img create -f qcow2 /home/cytech/VMdisk.qcow2 10G
```

## Booting a VM
``` ssh
kvm -hda <img file>
kvm -cdrom <iso file>
kvm -boot d -m 512 -netdev user,id=user.0,hostfwd=tcp::5555-:22 #port forwarding
kvm -device rtl8139,netdev=user.0
```

## Take snapshot
``` shell
qemu-img snapshot -I image.img
qumu-img -c snapshot image.img
```