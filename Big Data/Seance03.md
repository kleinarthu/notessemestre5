Contact du prof: luc.balluais@hupi.fr
# Big Data et Machine Learning
>[!Define] Big Data
>Discipline étudiant l'utilisation de gros volumes de données.

>[!Define] Machine Learning
>Partie du Big Data voué à la prédiction.

>[!Example] Applications du Machine Learning
>- Traduction automatique,
>- Reconnaissance vocale,
>- Reconnaissance d'images,
>- Prédiction de traffic,
>- Recommendation de produits,
>- Voitures autonomes,
>- Diagnostics médicaux,
>- Trading,
>- Détection de fraudes,
>- Assistance virtuel,
>- Filtrage des mails,
>- ...

>[!Example] Challenges associés
>- Comment récupérer les données,
>- Comment les stocker,
>- Comment les interpréter,
>- ...

>[!Define] Features
> Caractéristique d'un individu.

>[!Define] Samples
>Ensemble d'éléments dans une base de données.

>[!Define] Feature extraction
>Création de nouvelles caractéristiques à partir des caractéristiques des individus.

>[!Define] Training set
>Ensemble de données servant à l'entraînement du modèle.

# Spark
## But
Gérer de grandes quantités de données en utilisant le traitement parallèle.

## Langages de machine learning
- Python: plus polyvalent (dev web + scripting),
- R: plus mathématiques / statistiques,
- Scala: Analyse et préparation pour grands volumes de données mais manque d'outils,
- Java,
- js,
- C++,
- C#,
- bash,
- ts,
- Julia.

>[!Define] Lazy Framework
>Attends d'avoir toutes ses opérations pour les exécuter toutes d'un coup quand on demande l'affichage.

>[!Define] RDD
>Resilient Distributed Dataset: Ensemble de données immuables distribuées et partitionées sur plusieurs nœuds d'un cluster Spark.

# Machine Learning
- Supervisé: On connaît la variable cible (classification et régression),
- Non Supervisé: On ne connaît pas directement la cible (clustering et filtre collaboratif).

>[!Define] Recall
>vrai positif / positifs

>[!Define] Précision
>vrai positif / prédit positif

>[!Define] F-mesure
>${2 \cdot Recall \cdot Précision} \over { Recall + Précision }$