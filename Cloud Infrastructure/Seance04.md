# Dockerfile
The manual process to create Docker images is prone to errors.
The process can be automatized using a **Dockerfile**.
>[!Define] Dockerfile
>Build recipe for a Docker image.
## Create a Dockerfile
>[!Example] Dockerfile
>```
>FROM ubuntu
># This is a comment
>RUN apt-get update
>RUN apt-get install figlet
>CMD figlet -f script hallo
>```
>```
>FROM ubuntu
># This is a comment
>RUN ["apt-get", "update"]
>RUN ["apt-get", "install", " figlet"]
>CMD ["figlet", "-f", "script", "hallo"]
>```
## Use a Dockerfile
```shell
docker build <directory>
```

>[!Note] Override CMD
>CMD can be override with
>```shell
>docker run -it <command>
>```

>[!Important]
>If nothing has been changed, the image will not be re-built unless the --no-cache option is present.
## View the layers of an image
```shell
docker history <image name>
```

## Create parameters
```
ENTRYPOINT ["figlet", "-f", "script"]
```
Similaire à CMD mais les arguments donnés seront ajoutés à la fin de cette commande
### Default message
```
ENTRYPOINT ["figlet", "-f", "script"]
CMD["hello world"]
```
### Override a command if a parameter is wanted
```shell
docker run -it --entrypoint bash figlet
```
## Copy a file
```
COPY <file> <dest>
```
## Give the author name
```
MAINTAINER Arthur Klein <kleinarthu@cy-tech.fr>
```

## Tell what ports are to be published
```
EXPOSE 8080
EXPOSE 80 443
EXPOSE 53/tcp 53/udp
```
>[!Important]
>All ports are private by default.
### Make a port public
```shell
docker run -p <port>
```

```shell
# Make all exposed ports public
docker run -P
```

## Difference between COPY and ADD
- Add can get remote files,
- Add automatically unpack zip files.

## Change the current directory
```
WORKDIR /src
```

## Set environment variables
```
ENV WEBAPP_PORT 8080
```

```shell
docker run -e WEBAPP_PORT=8080
```

## Change the user
```
USER <uid>
```
## Get the info in json format
``` shell
docker inspect
```

# Networking
## Show the exposed ports
```shell
docker port <container id> <port>
```

## Set the port manually
```shell
docker run -d -p 80:80 nginx
```

## Get the ip address
```shell
docker inspect --format `{{ .NetworkSettings.IPAddress }}` <container id>
```

## Set the ip
```shell
docker network create --subnet 10.66.0.0/16 pubnet
docker run --net pubnet --ip 10.66.66.66 -d nginx
```

## See and manipulate the networks
```shell
docker network ls
```

## Specify an alias
```shell
docker run --net localnet --net-alias redis -d redis
```

>[!Info] Other options
>- --internal: disables outbound traffic,
>-  --gateway: indicates which address to use for the gateway,
>-  --subnet: indicates the subnet to use,
>-  --ip-range: indicates the subnet to allocate from,
>-  --aux-address: allows to specify a list of reserved addresses,
>-  --ip: set the ip address (should be in the subnet),
>- --net: set the driver.

## Driver
### Define the driver
```shell
docker run --net <nom>
```

By default, the containers gets a virtual eth0 interface connected to docker0.
### Change the Docker bridge
--bridge
### Remove the interface
```shell
docker run --net none
```

### Give access to the whole network of the host
```shell
docker run --net host
```

# Volumes
>[!Define] Volume
>Mount a directory from the host to the container.
## Define a volume
```
VOLUME /uploads
```

```shell
docker run -d -v /uploads myapp
```

>[!Warning]
>When comiting, the volume does not go in the container.
## Copy the volume of a container for an other one
```
docer run --volumes-from <container> 
```

## List the volumes
```shell
docker volume ls
```

## Create a volume without a container
```shell
docker volume create --name=<nom>
```

## Get information
```shell
docker volume inspect <nom>
```

## Use a named volume
```bash
docker run -d -p 8888:80 -v <nom>:<path>
```

## Share a directory
```shell
docker run -d -v <path on the host>:<path on the container> <image>
```

# Docker compose
>[!Define] Docker compose
>Create multiple container and relate them, using a docker-compose.yml file.
>Recreate the volumes and container, ...
## Run a Docker compose
```shell
docker-compose up
```
Recreate the environment.

## Trainingwheels example
>[!Example] docker-compose.yml
>```yml
>version: "3"
>
>services:
>  www:
>    build: www
>    ports:
>      - ${PORT-8000}:5000
>    user: nobody
>    environment:
>      DEBUG: 1
>    command: python counter.py
>    volumes:
>      - ./www:/src
>
>  redis:
>    image: redis
>```
- Creates two container (www and redis),
- build gives the path to a Dockerfile.
- image gives the path to an image.

>[!Info] Documentation
>[version 2](https://docs.docker.com/compose/compose-file/compose-file-v2/)
>[version 3](https://docs.docker.com/compose/compose-file/)

## Other commands
### Run docker build
```shell
docker-compose build
```
### Start in the background
```shell
docker-compose -d up
```
### See the status of the container in the current stack
```shell
docker-compose ps
```
### Kill a background application
```shell
docker-compose kill
```
### Remove Containers
```shell
docker-compose rm
```

```shell
docker-compose down
```

>[!Important] TP noté
>TP6 Part 3
>Will have to be sent around march