# Pointeurs intelligents
>[!Define] Box
>Pointeur vers une valeur dans le tas.

>[!Define] RC
>Pointeur permettant de partager plusieurs fois une même variable.

>[!Define] Refcell
>Permet de faire ce que l'on veut sur un pointeur.

>[!Define] Arc
>RC pour du code asynchrone.

>[!Define] Mutex
>Refcell pour du code asynchrone.

>[!Define] Cow
>Clone on write: Prête les variables de manière immutable et les clone quand elles ont besoin de l'ownership.