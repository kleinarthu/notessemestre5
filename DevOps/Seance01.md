Si un élève a un besoin particulier, possibilité de faire un cours spécifique dessus.
# Introduction du DevOps
## Définition
>[!Define] DevOps
>Development Operation: fournir les outils aux développeurs pour l'opération.

- Accélère le déploiement,
- Automatise le déploiement de l'infrastructure,
- Réduire le time-to-market.

Créé par Patrick Degois et Andrew Shafer et popularisé en 2008.

## Pourquoi les DevOps?
Grosse séparation entre les équipes de développeurs et les équipes de déploiement:
- mauvaise communication,
- bugs,
- beaucoup d'échecs.
Objectifs séparés (fonctionnalité vs coûts).
Responsabilité différentes.

=> Lier les deux.

## Conséquences
- Améliorer les livrables,
- Possibilité de faire du déploiement continu (tests, ... à chaque modification pour un passage en prod plus rapide),
-  Résolution des tickets plus rapide,
- Maîtrise des outils et projets partagée par plus de membres,
- Meilleure connaissance de la dette technique.

>[!Define] TJM
>Taux journalier moyen: payment chaque jour (400-1400€).

>[!Define] Test d'intégration
>On vérifie qu'intégrer un module ne modifie pas le fonctionnement du reste du code.

## CALMS
### Culture
Faire la liaison entre des groupes qui ne savent pas de quoi parlent l'autre groupe.

### Automation
Tout ce qui peut être automatisé doit être automatisé.

### Lean
Prôner la transparence et la réactivité.
-> Admettre ses erreurs et chercher des solutions en groupes.

### Metrics
Tout mesurer (Combien de déploiements par jour? Quel pourcentage de succès est attendu?)

>[!Define] SLA
>Service level agreement: A-t'on la possibilité de réussir à répondre à une requête avec un certain pourcentage?

### Sharing
Partager l'expérience, autant pour les succès que les échecs.

## Lien avec l'agile
Même philosophie:
- Importance de l'amélioration du produit,
- Importance de la résolution des problèmes,
- Importance d'avoir une même vision pour le produit et l'équipe,
- Importance de la relation entre les membres de l'équipe (sur les domaines techniques maîtrisés par chacun).

## Automatisation
### Objectifs
- Fainéants,
- Possibilité de faire des erreurs.

# Agilité
## Origine
Standish a remarqué que 64% des fonctionnalités d'une application sont peu ou jamais utilisées (dont 45% qui ne sera jamais utilisé).

## Projet traditionnel
### Mesure de la réussite du projet
- Réussite du budget,
- Réussite du planning,
- Réussite du cahier des charges.

Mais n'a pas forcément de relation avec la satisfaction du client.
-> Plus lié à l'écoute et à la réactivité.
![](https://www.process.st/wp-content/uploads/2018/10/waterfall-v-agile-iron-triangle-v03.png)

## Caractéristiques de la méthode agile
### Définition
>[!Define] Agile
>= Mélanger l'itératif et l'incrémental:
>- **itératif** = affiner à chaque itération,
>- **incrémental** = ajouter des nouvelles fonctionnalités à chaque itération.

### Manifeste
- Valoriser les relations entre les individus et la collaboration plutôt que les outils et les contrats.
- Plus de logiciels opérationnels et moins de documentation externe,
- S'adapter aux changements,
- 12 Principes.
![](https://wikiagile.fr/images/thumb/d/d3/The12AgilePrinciples_fr.png/1000px-The12AgilePrinciples_fr.png)


## Scrum
Comme en mêlée au rugby, on avance tous vers un objectif commun.
>[!Define] Moto de scrum
>Baser toute la connaissance d'une équipe sur ce qu'elle a vécu auparavant.

-> Ne pas ré-itérer les mêmes erreurs.
### Piliers
- Transparence,
- Inspection,
- Adaptation.

### Valeurs
- Courage,
- Focus,
- Commitment,
- Respect,
- Openness.

### Rituels
- Daily,
- Sprint planning,
- (Sprint),
- Backlog refinement,
- Sprint review,
- Retrospectives.

### Artifacts
- Product backlog (tous les tickets écrits pour le produit),
- Sprint backlog (même chose mais sur un sprint),
- Product increment (livrable à la fin du sprint).

### Rôles
- Product owner (Reçoit le besoin du client),
- Scrum master (S'assure que les principes sont respectés),
- Team.

Stakeholders = clients

# CI CD
>[!Define] CI CD
>L'art d'automatiser, de se rendre la vie simple.

## CI
>[!Define] CI
>Intégration continue: améliorer le code en direct.

Nécessités:
- code source partagé,
- les développeurs sont disponibles en permanence,
- les tests sont écrits par les développeurs.


### Outils
>[!Define] Jira
>Outil pour donner du contexte à ce qui a été écrit dans le code, écrire des tickets.

>[!define] Github
>Permet de stocker le dépôt et d'ajouter des commentaires.

>[!Define] Sonarqube
>Permet de vérifier plein de chose (détection automatique).
>-> État de santé de la codebase

>[!define] Jenkins
>Outil de pipeline pour lancer une série de job à chaque fois qu'une action est effectuée.

>[!Define] Github Action
>Comme Jenkins mais moins step by step.

## CD
>[!Define] CD
>Continuous deployment: On release ce qui a été CI.

Après un pipeline de vérification.
Même version de l'application partout.

### Outils
- Jenkins,
- Github Action.

>[!Failure] Inconvénients
>- Investissement initial conséquent.

>[!Success] Avantages
>- Qualité des livrables,
>- Releases rapides,
>- Risques faibles,
>- Satisfaction client et utilisateur.

## Pipeline
>[!Define] Pipeline
>Ensemble de tâches récursives automatisées pouvant être séquenteilles ou parallèles permettant d'effectuer les opérations nécessaires en réponse à une modification pour vérifier le fonctionnement du programme.

### Exemple
Dans une VM:
1. Compilation / Checkout,
2. TU (test unitaire) / TI (test d'intégration) / E2E (end to end),
3. Packaging,
4. Déploiement,
5. Validation,
6. Tests de non régression.

Pareil pour l'infrastructure, puis le back-end, puis le front-end.

### Caractéristiques
- Peut recevoir des paramètres de configuration,
- Peut définir des variables internes,
- Plusieurs niveaux d'exécution: Stages > Jobs > Steps > Tasks,
- Dépend du système utilisé,
- Langage déclaratif (mais Turing complet): **Turing**.

## Stratégies de flux Git
Branches:
- Master (version distribuée),
- Hotfix (correction urgente de la master),
- Develop (branche principale, celle que l'on veut modifier),
- Feature (branche temporaire pour faire des modifications),
- Release (capture à un instant T de develop pour mettre à jour Master).

>[!Info] Liens utils
>[Lien vers la doc de github action](https://docs.github.com/en/actions)
>[Exemple](https://github.com/ia-z/ia-z)