# Introduction to Cloud Computing and Virtualization

## Traditional approach
single user / DMZ / LAN
Then every services was put in a virtual machine assembled in a cluster.

>[!Define] Cluster
>Ensemble of virtual services in a physical infrastructure.

## Cloud approach
>[!Define] Cloud Computing
>**Cloud computing** is a model for enabling ubiquitous, convenient **on-demand network access to a shared pool of configurable computing resources**.

## Virtualization
>[!Define] Virtualization
>Act of creating a virtual version of something, including virtual computer hardware platforms, storage devices, and computer network resources.

Possible through an **hypervisors**.

### Types of virtualization
The guest OS no longer runs in ring 0.

#### Type 1: sort of operating system
>[!Define] Binary translation
>Translating every command of the guest OS on the file -> really slow.

>[!Define] Paravirtualization
>Replacement of "critical" of "dangerous" code to safe code in the source code.
>You recompile the kernel in the machine.

#### Type 2: new window
>[!Define] Hardware Virtualization
>Capture the privileged instructions, traps all exception and translate them to something that will be executed by the OS.

## Service models
Infrastructure Provider
n*(Hardware <-> OS <-> Virtualization Layer) <-> Infrastructure Management <-> n*(Service (for users)|Infrastructure Interface (for service provider)))

### IaaS
You provide an access to create virtual machines to have a small cluster (aws).

### PaaS
For example developing platform as a Service.

### SaaS
Software that runs on the cloud instead as on your computer (gmail).

## Deployment models
Private Cloud / Public Cloud / Hybrid Cloud

## Cloud providers and services
- AWS,
- Azure,
- Google Cloud,
 - Oracle.

Many services are similar between cloud providers(cloud storage, ...).

## Hidden cost
- Free Tier Antiquity,
- Under-utilized Elastic IP address,
- Unused elastic load balances,
- Unused Elastic block storage,
- Orphan elastic block storage snapshots,
- SI Access charges,
- Si partial uploads,
-  Transfer cost (out of the service).

## Introduction to OpenNebula
>[!Define]
>Uniform management layer in charge of orchestrating different technologies.

![](https://www.opensourceforu.com/wp-content/uploads/2017/01/Figure-1-OpenNebula-deployment-model.jpg)
# Virtualization with KVM (Kernel Virtual Machine)
Hardware Virtualization
## Verify that the VT-x virtualization is enabled
``` shell
grep --color vmx /proc/cpuinfo
```

## Installation
``` shell
sudo apt install qemu-kvm
sudo apt install virt-manager
```

## Supported image formats
- Qcow2,
- Raw.

```ssh
qemu-img create -f qcow2 /home/cytech/VMdisk.qcow2 10G
```

## Booting a VM
``` ssh
kvm -hda <img file>
kvm -cdrom <iso file>
kvm -boot d -m 512 -netdev user,id=user.0,hostfwd=tcp::5555-:22 #port forwarding
kvm -device rtl8139,netdev=user.0
```

## Take snapshot
``` shell
qemu-img snapshot -I image.img
qumu-img -c snapshot image.img
```
# Docker
## Problems with the deployment of software
>[!Failure] Deployment of software
>- Heavy softwares,
>- Hard to make a new version.

>[!Success] With contianer
>-  Decoupled services,
>- Fast improvements.

## Container
>[!Define] Container
>Isolates a running application into an execution context.

>[!Success] Advantages
>- Consistent en persistent infrastructure,
>- Less overhead compared to VM,
>- Isolated and automated.

## Difference between VM and Container
>[!Define] VM
>- Infrastructure,
>- Host Operating System,
>- Hypervisor,
>Per VM:
>- Guest OS,
>- Bins/Libs,
>- App.

>[!Define] Docker
>- Infrastructure,
>- Operating System,
>- Docker Engine,
>Per container:
>- Bins/Libs,
>- App.

>[!Failure] Inconvenient
>Can not have different OS between containers.
## Configuration
```shell
docker version
sudo groupadd docker
sudo gpasswd -a $USER docker
sudo service docker restart
```

## Change Docker network
```shell
sudo vim /etc/docker/daemon.json
```

## Docker architecture
- **Docker Engine**,
- **Docker Client**,
- **Docker Hub Registry**.

## Lancer docker
``` shell
docker run -it ubuntu /bin/bash
# Ubuntu sera téléchargé depuis les librairies de Docker
```

## Stop the container
``` shell
exit
```

## List the containers
```shell
docker ps
# To also check stopped
docker ps -a
docker start <container id>
docker attach <container id>
```

Containers can be non-interactive and run forever in the background.
## Non Interactive container
``` shell
docker run -d jpetazzo/clock
```

## Kill a container
```shell
docker kill <container id>
```

## Stop a container
```shell
docker stop <container id>
# 10 sec before TERM
```

## Detach from an interactive container

```shell
docker run -ti --detach-key ctrl-x,j <container>
# Specify the touch to stop the container
```

## Debugging
``` shell
docker exec <container>
```

## Docker images
>[!Define] Image
>- Collection of files + metadata,
>- Root filesystem of a container,
>- Read-only files,
>- Stack of layers adding, changing or removing files,
>- Able to share some layers between different images.

### How to change an image
1. Create a container with the image,
2. Do the modifications,
3. Create a new image from this container with:
```shell
docker commit
docker build
```

### Check the images available
```shell
docker images
```

### Download an image
```shell
docker pull wordpress:cli-1.2
```

### Remove a container
```shell
docker rm <container id>
```

### Remove an image
```shell
docker rmi <image id>
# only works if no container use this image.
```

### Build image
```shell
# Check difference
docker diff containerID

docker commit containerID image_name
```
# Dockerfile
The manual process to create Docker images is prone to errors.
The process can be automatized using a **Dockerfile**.
>[!Define] Dockerfile
>Build recipe for a Docker image.
## Create a Dockerfile
>[!Example] Dockerfile
>```
>FROM ubuntu
># This is a comment
>RUN apt-get update
>RUN apt-get install figlet
>CMD figlet -f script hallo
>```
>```
>FROM ubuntu
># This is a comment
>RUN ["apt-get", "update"]
>RUN ["apt-get", "install", " figlet"]
>CMD ["figlet", "-f", "script", "hallo"]
>```
## Use a Dockerfile
```shell
docker build <directory>
```

>[!Note] Override CMD
>CMD can be override with
>```shell
>docker run -it <command>
>```

>[!Important]
>If nothing has been changed, the image will not be re-built unless the --no-cache option is present.
## View the layers of an image
```shell
docker history <image name>
```

## Create parameters
```
ENTRYPOINT ["figlet", "-f", "script"]
```
Similaire à CMD mais les arguments donnés seront ajoutés à la fin de cette commande
### Default message
```
ENTRYPOINT ["figlet", "-f", "script"]
CMD["hello world"]
```
### Override a command if a parameter is wanted
```shell
docker run -it --entrypoint bash figlet
```
## Copy a file
```
COPY <file> <dest>
```
## Give the author name
```
MAINTAINER Arthur Klein <kleinarthu@cy-tech.fr>
```

## Tell what ports are to be published
```
EXPOSE 8080
EXPOSE 80 443
EXPOSE 53/tcp 53/udp
```
>[!Important]
>All ports are private by default.
### Make a port public
```shell
docker run -p <port>
```

```shell
# Make all exposed ports public
docker run -P
```

## Difference between COPY and ADD
- Add can get remote files,
- Add automatically unpack zip files.

## Change the current directory
```
WORKDIR /src
```

## Set environment variables
```
ENV WEBAPP_PORT 8080
```

```shell
docker run -e WEBAPP_PORT=8080
```

## Change the user
```
USER <uid>
```
## Get the info in json format
``` shell
docker inspect
```

# Networking
## Show the exposed ports
```shell
docker port <container id> <port>
```

## Set the port manually
```shell
docker run -d -p 80:80 nginx
```

## Get the ip address
```shell
docker inspect --format `{{ .NetworkSettings.IPAddress }}` <container id>
```

## Set the ip
```shell
docker network create --subnet 10.66.0.0/16 pubnet
docker run --net pubnet --ip 10.66.66.66 -d nginx
```

## See and manipulate the networks
```shell
docker network ls
```

## Specify an alias
```shell
docker run --net localnet --net-alias redis -d redis
```

>[!Info] Other options
>- --internal: disables outbound traffic,
>-  --gateway: indicates which address to use for the gateway,
>-  --subnet: indicates the subnet to use,
>-  --ip-range: indicates the subnet to allocate from,
>-  --aux-address: allows to specify a list of reserved addresses,
>-  --ip: set the ip address (should be in the subnet),
>- --net: set the driver.

## Driver
### Define the driver
```shell
docker run --net <nom>
```

By default, the containers gets a virtual eth0 interface connected to docker0.
### Change the Docker bridge
--bridge
### Remove the interface
```shell
docker run --net none
```

### Give access to the whole network of the host
```shell
docker run --net host
```

# Volumes
>[!Define] Volume
>Mount a directory from the host to the container.
## Define a volume
```
VOLUME /uploads
```

```shell
docker run -d -v /uploads myapp
```

>[!Warning]
>When comiting, the volume does not go in the container.
## Copy the volume of a container for an other one
```
docer run --volumes-from <container> 
```

## List the volumes
```shell
docker volume ls
```

## Create a volume without a container
```shell
docker volume create --name=<nom>
```

## Get information
```shell
docker volume inspect <nom>
```

## Use a named volume
```bash
docker run -d -p 8888:80 -v <nom>:<path>
```

## Share a directory
```shell
docker run -d -v <path on the host>:<path on the container> <image>
```

# Docker compose
>[!Define] Docker compose
>Create multiple container and relate them, using a docker-compose.yml file.
>Recreate the volumes and container, ...
## Run a Docker compose
```shell
docker-compose up
```
Recreate the environment.

## Trainingwheels example
>[!Example] docker-compose.yml
>```yml
>version: "3"
>
>services:
>  www:
>    build: www
>    ports:
>      - ${PORT-8000}:5000
>    user: nobody
>    environment:
>      DEBUG: 1
>    command: python counter.py
>    volumes:
>      - ./www:/src
>
>  redis:
>    image: redis
>```
- Creates two container (www and redis),
- build gives the path to a Dockerfile.
- image gives the path to an image.

>[!Info] Documentation
>[version 2](https://docs.docker.com/compose/compose-file/compose-file-v2/)
>[version 3](https://docs.docker.com/compose/compose-file/)

## Other commands
### Run docker build
```shell
docker-compose build
```
### Start in the background
```shell
docker-compose -d up
```
### See the status of the container in the current stack
```shell
docker-compose ps
```
### Kill a background application
```shell
docker-compose kill
```
### Remove Containers
```shell
docker-compose rm
```

```shell
docker-compose down
```

>[!Important] TP noté
>TP6 Part 3
>Will have to be sent around march
# Compte google
>[!Important] Compte google
>S'assurer de ne pas être sur son compte personnel.

>[!Important]
>Bien éteindre les VM non utilisées pour ne pas dépenser le crédit de son compte.

# Kubernetes: Container orchestration
>[!Failure] Problem
>Manually managing several container with their networks can become difficult.

=> **Kubernetes** (= K8s)
>[!Define] Kubernetes
>Open source system to manage clusters of container.

## Architecture
>[!Define] Defined state
>Specified by the developer in a yaml or json file they submit to the API,

>[!Define] Actual state
>Compared to the Defined state by Kubernetes do update it.

>[!Define] Pods
>Group of one or more containers that share network and storage, managed as a single unit.

>[!Define] Node
>Worker machine (physical or virtual) in the cluster.

>[!Define] Master node
>Node that chose how to manage the network for the other nodes.

>[!Define] Deployment
>Update the actual state with the desired state.

The master automatically re-create the deleted or closed nodes.
## GCP (Google Cloud Platform)
Used for the ML of Google
## Pod
- Can run one or more container,
- Share networking and storage separate from the node,
- Defined with a YAML file.
>[!Example] pod.yaml
>```yaml
>apiVersion: v1
>kind: Pod
>metadata:
>	name: my-app
>spec:
>	containers:
>	- name: my-app
>	  image: my-app
>	- name: ngix-ssi
>	  image: ngix
>	  ports:
>	  - containerPort: 80
>	  - containerPort: 443
>```

## Deployment
- Ensure the number of pods running at the same time,
>[!Example] deployment.yaml
>```yaml
>kind: Deployment:
>apiVersion: v1.1
>matadata:
>	name: frontend
>spec:
>	replicas: 4
>	selector:
>		role: web
>	template:
>		metadata:
>			name: web
>			labels:
>				role: web
>		spec:
>			containers:
>			- name: my-app
>			  image: my-app
>			- name: ngix-ssi
>			  image: ngix
>			  ports:
>			  - containerPort: 80
>			  - containerPort: 443
>```

## Service
>[!Define] Service
>Abstract way to expose an application running on a set of pods as a network service.

>[!Example] service.yaml
>```yaml
>kind: Service
>apiVersion: v1
>metadata:
>	name: web-frontend
>spec:
>	ports:
>	- name: http
>	  port: 80
>	  targetPort: 80
>	  protocol: TCP
>	selector:
>		role: web
>	type: LoadBalancer
>```

## Labels
>[!Define] Labels
>Key-value pairs used in the pods.

>[!Example] labels.yaml
>```yaml
>"metadata": {
>	"labels": {
>		"key1": "value1",
>		"key2": "value2"
>	}
>}
>```