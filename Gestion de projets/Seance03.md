# Lean
>[!Define] Lean
>Pensée qui se concentre sur la recherche de performance au sein d'un projet.

Originaire du Japon

## 5 P
- Production de valeur du produit,
- Processus: flux de valeur,
- Performance: Pull/JIT,
- Personnes: Respect - Autonomie,
- Perfection.

=>
- Production en flux tendu,
- Travail standardisé pour éliminer les trois démons:
   - Muda (gaspillage),
   - Muri (surcharge),
   - Mura (variation).

>[!Define] Robuste
>Nécessite une stabilité de :
>- Main d'oeuvre,
>- Méthodes,
>- Moyens,
>- Matières.

>[!Define] Sûreté
>Englobe FDMS:
>- Fiabilité,
>- Disponibilité,
>- Maintenabilité,
>- Sécurité.

## 5S
- Seiri: Débarasser (de l'inutile),
- Seiton: Ranger,
- Seiso: Standardiser,
- Seiketsu: Standardiser,
- Shitsuke: Respecter.

## Workflow git
- Gitflow (Créer des nouvelles branches pour les fonctionnalités),
- Github flow (Créer des forks pour les fonctionnalités),
- Trunk (Tout mettre dans la branche principale).

## Kaïzen
>[!Define] Kaïzen
> Changer pour le mieux, amélioration continu (plutôt que de tout refaire).

## Muda
>[!Define] Muda
>Gaspillage

**WORMPIT**:
- Waiting: Les attentes,
- Over Production: Sur-production,
- Rejects: bug,
- Motion: multi-tasking,
- Processing: Sur-qualité,
- Inventory: Tâches non-terminées,
- Transport: Mauvaise communication.

## Muri
>[!Define] Muri
>Déraisonnable, excessif.

Utiliser du matériel qui fournit plus de ressources que celles nécessaires.

## Mura
>[!Define] Mura
>Irrégulier, qui manque d'uniformité.

Alterner entre sur et sous soliciter une équipe ou une ressource.

## Heijunka
>[!Define] Heijunka
>Lissage

Lissage la production en termes de volume.
Lisser la variété des produits (ou projets).
 -> Moins de sur-sollicitation, productivité plus prévisible, opposé au pull, évite la perte de temps pour les changements.

## Genchi Genbutsu
>[!Define] Genchi Genbutsu
>Le vrai lieu, la vraie chose.

L'analyse des problèmes se fait sur le terrain.

**Gemba Walk**: Anticiper en allant sur le terrain avant que les problèmes arrivent.

## Métriques
>[!Define] Process Time
>Temps passé sur un produit sans les temps d'attente et les temps intermédiaires.

>[!Define] Cycle Time
>Temps passé sur un produit avec les temps d'attente et les temps intermédiaires.

>[!Define] Lead Time
>Temps entre la demande et sa livraison.

>[!Define] Takt Time
>Temps moyen de production sur une période courte. (équivalent de la vélocité en SCRUM)

## Jidoka
>[!Define] Jidoka
>Autonomation = Autonomie + Automatisation

Automatisation maximale, mais avec un opérateur qui garde le contrôle sur la machine.

## Andon
>[!Define] Andon
>Système d'alarme qui permet à un opérateur de signaler à tout moment qu'il y une anomalie.

-> Contrôle par chaque travailleur,
-> Réactivité.

## Poka-Yoke
>[!Define] Poka-Yoke
>Anti-erreur: Système permettant d'empêcher physiquement les erreurs.

## Qualité Totale
>[!Define] Qualité Totale
>Il doit y avoir de la qualité dans tout ce que l'on fait (tous les produit, tous les process, ...).

-> Contrôle continu de la qualité.

## Kanban
>[!Define] Kanban
>Étiquette: fiche cartonnée qu'on fixe sur les bacs ou les conteneurs de pièces pour contrôler les stocks entrants.

>[!Define] Limites WIP
>Limiter le travail en cours en limitant le nombre de tâches que l'on peut mettre dans une colonne.

## Hypothesis-Driven Development
>[!Define] Hypothesis-Driven Development
>Façon de rédiger les US.

Nous croyons que si on ...
On obtiendra ...
Nous poursuivrons dans cette voie si nous mesurons ...

## DevOps
>[!Define] DevOps
>Faire travailler ensemble Développeurs et Opérateurs.

## Lead Time to Changes
- Équipe Élite: < 1j,
- Équipe performante: 1j à 1sem,
- Équipe moyenne: 1 sem à 1 mois,
- Éuipe médiocre: 1 à 6 mois.
## Deployment Frequency
- Équipe Élite: Plusieurs fois par jour,
- Équipe performante: 1 fois/j,
- Équipe moyenne: 1 fois/sem,
- Équipe médiocre: 1 fois/mois.

## Change Failure Rate
- Élite/Haute/Moyens: 0-15%,
- Médiocres: 40-60%.

## Mean Time to Restore
- Équipe Élite: <1h
- Éuipe performante & moyenne: <1j,
- Équipe médiocre: 1 sem à 1 mois.

## CICD
>[!Define] Continuous Integration
>Joue automatiquement et régulièrement tous les tests.

>[!Define] Continuous Delivery
>Package et stocke le livrable prêt au déploiement dans un repository d'artefacts.

>[!Define] Continuous Deployment
>Déploie automatiquement une app/lib dans l'environnement adéquat.