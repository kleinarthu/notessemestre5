# Stage
Début de stage
2 avril
22 semaines - 6 mois

Forum: 30 Novembre (concerne tout le monde)

>[!Info] Alternants uniquement
>Fiche de présence par demi-journée à remplir par tous les alternants et l'intervenant.
>Transmettre un arrêt de travail en cas d'abscence.
>Jours de congés en dehors des jours école.
>Informations sur Teams.

# Votes sur [wooclap](wooclap.com)
- Accessibilité et virtualisation importantes,
- Amazon à l'origine,
- AWS sera la principale platforme utilisée (GCP un peu utilisé mais pas Azure),
- Domaine qui évolue d'une année sur l'autre (il faut être curieux).

# Définitions
>[!Define] Cloud Computing
>Cloud Computing consists in outsourcing, on the internet, the IT, software, storage, and more services... to a provider, offering pay-as-you-go pricing.

>[!Attention]
>50$ sur les comptes google cloud (fermer la machine virtuelle quand non utilisée)

>[!Define] Scalabilité
>Capacité de gérer plus de données, de calculs sans dégrader les performances.

>[define] aaS
>Écosystème fournissant plus ou moins:
>- IaaS: Infrastructure as a Service (host, acheter des machines vides, tout est à configurer),
>- Paas: Platform as a Service (build, la plateforme s'occupe d'héberger mes logiciels et de les faire tourner),
>- SaaS: Software as a Service (consume, je ne fais qu'utiliser un logiciel pré-existant comme gmail).
>Mais aussi DaaS, TEaas, SEaas,...

# Programme de l'année

## Validation
Fiche ECTS disponible dans ce dépôt.
Avoir au moins 10 de moyennes dans chaque UE.
Deux jurys en octobre et janvier.

## Prérequis
- Programmation orientée objet,
- Programmation fonctionnelle,
- Programmation système,
- Test et vérif,
- Développement web, asynchrone,
- Complexité,
- Base de données,
- Anglais,
- Réunion/entretien,
- Rapport, autonomie, ...


## Cloud Perspectives 1
Présenter à l'oral un sujet en groupe dans un "mini-cours" d'un quart d'heure.

## Liens utiles
Recherche:
- [ResearchGate](researchgate.net),
- [Hal](hal.science),
- [GoogleScholar](scholar.google.com).

Podcasts:
- castcodeurs,
- cloudcast,
- AWS.

Youtube:
- Devoxx,
- DevoxxFR,
- DevFest,
- Ted.

>[!Info]
>Cours Cloud Data 2 sur Data pipline et snowflake remplacé par un projet.

# PFE (Projet de Fin d'Étude)
- Orienté R&D,
- ambitieux (même si ça n'aboutit pas).
- 29 avril pour les contrats pro, soutenance le 14 juin,
- 22 décembre pour les étudiants, soutenance le 23 février.

# Élection des délégués
Un délégué et un suppléant (choisir avant la fin de la semaine).