# Propositions de sujets
- **émulateur GameBoy**,
- redox-OS -> Pull Request,
- lab test malware,
- faire un système de log pour des modifications fonctionnelles, et d'analyse pour trouver des anomalies.

# Threads
>[!Define] Process
>- Géré au niveau kernel,
>- Lourd,
>- Créé par fork() sous Linux et CreateProcessA sous Windows.

Pas de runtime en rust par défaut.
Donc les threads sont des threads noyaux.
## Instanciation
```rust
use std::thread;
fn main() {
	thread::spawn(move || {
		// work
	});
}
```
## Communication avec les threads
### Mémoire partagée
```rust
use std::sync::mpsc;
fn main() {
	let (tx, rx) = mpsc::channel();
}
```
## Équivalent asynchrone de Rc\<Refcell
```rust
Arc<Mutex<>>
```

# Slice
>[!Define] Slice
>Morceau de collection.

>[!Example]
>- &str (pour string),
>- &\[T\] (pour Vec),

# Date de rendu du projet
13/10