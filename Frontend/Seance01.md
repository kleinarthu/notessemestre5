# Nom du professeur
**Gérald Abbadie**
# Langage utilisé
**Angular**
- Vérifier la version de node,
- Vérifier l'installation de npm,
- Installer angular: https://angular.io/cli.
**Firebase** en fin de module.
# Questionnaire: État des lieux préalable
## ECMAScript
> [!Define] ECMAScript
>Norme logicielle de W3C qui dicte les normes de javascript pour assurer la rétro-compaptibilité.
## Contexte et binding en Javascript
Le moteur (eg V8) s'injecte dans un environnement. Quel que soit l'environnement, il va tourner, être léger, utiliser peu de ressources... Pour ce faire, il a besoin d'un moyen de dialoguer avec le système (eg nodeJS).
# Sujet de l'application
**Gestionnaire de soirée.**

# Utilité des fichiers du projet
## tsconfig.json
Configure typescript (pour avoir du code plus robuste).
Normalement pas besoin d'y toucher (sauf si problèmes de versions).
## package.json
Dépendances du projet pour une application node.
Distingue les dépendances en développement et celles de l'hôte.
Elles sont installées dans **node_modules**.
## angular.json
Fichier de paramétrage de l'application.
## src/assets/
Fichier qui seront rajoutés dans l'appli (images, ...).
## src/index.html
N'est pas modifier dans 99% des cas.
## src/main.ts
Point d'entrée de l'application.
## src/app/
Dossier dans lequel on va travailler.
## src/app/app-module.js
Gestion des modules.
# Concepts importants
> [!Define] Modules
>Liste de classes référencées et réparties dans l'application.
>= librairie

>[!Define] Component
>Sert à afficher une page ou un morceau de page.

# Utilisation
## Mise à jour de la page
Angular modifie la page automatiquement quand le code est modifié.
## Utilisation des variables
Il est possible de récupérer les variables du ficher ts dans le fichier html associé en utilisant des "moustaches": **{{ }}**
## Typage
```ts
nom: string = "klein";
listeMoustache: Array<string> = [];
```
## Méthodes
```ts
nom() {
}
```
## Constructeur
```ts
constructor() {
}
```
## Créer un composant
```shell
ng generate component template/footer
# ng g c template/footer
```
## Créer un modèle
```ts
ng g interface shared/models/usersI
```

# Décisions sur le projet
## Objets
- User,
- Profile,
- Evenement,
- Inscription,
- Organisation: stock, creer_event

Il faut avoir un certain statut pour créer un événement.
Il faut que les utilisateurs soient gérés par un autre super-utilisateur.

## Rôles
- Administrateur (créer des événements),
- Inscrits (a un profile, peut s'inscrire à un événement),
- Autre utilisateur.
