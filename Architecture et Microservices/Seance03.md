# FastAPI

>[!Question] Pourquoi Fast?
>- Rapide,
>- Développement rapide,
>- Beaucoup de choses gérées par le framework -> moins de bugs,
>- Facile à apprendre,
>- Production ready,
>- RESTful.
## Installation
```shell
pip install fastapi
```

## Example
>[!Example]
>```python
>from fastapi import FastAPI
>
>app = FastAPI()
>
>@app.get("/")
>async def root():
>    return {"message": "Hello World"}
>
>@app.get("/items/{item_id}") # Get a parameter
>async def read_intem(item_id: int):
>    return {"item_id": item_id}
>
>@app.get("/items/")
>async def read_item(skip: int = 0, limit: int = 10): # Optional parameters
>    return fake_items_db[skip: skip + limit]
>
>```

## Lancer mon api
```shell
uvicorn main:app --reload
```

## Documentation de mon api
>[!infor] Documentation de mon api
>- [localhost:8000/doc](localhost:8000/doc)
>- [localhost:8000/redoc](localhost:8000/redoc)

 >[!Definition] ORM
 >Object Relational Mapping

## Définir le modèle en sortie et entrée
```python
from typing import Optional
from fastapi import FastAPI
from pydantic import BaseModel

app = FastAPI()

class UserIn(BaseModel):
	username: str
	password: str
	email: EmailStr
	full_name: Optional[str] = None

class UserOut(BaseModel):
	username: str
	email: EmailStr
	full_name: Optional[str] = None

@app.post("/user/", response_model = UserOut, status_code=status.HTTP_201_CREATED) # Returned code if success
async def create_user(user: UserIn):
	return user
```

## Communiquer du binaire
Sert pour images, etc ...

```python
from fastAPI import File
```

## Renvoyer une erreur
```python
from fastapi import HTTPException

raise HTTPException(status_code = 404, detail = "Item not found")
```

## Middleware

```python
@app.middleware("http")
async def add_process_time_header(request: Request, call_next):
	start_time = time.time()
	response = await call_next(request)
	process_time = time.time() - start_time
	response.header["X-Process-Time"] = str(process_time)
	return response
```

## CORS
>[!Definition] CORS
>**Cross Origin Ressource Sharing**: Filtrer qui a le droit de communiquer avec moi

```python
from fastapi.middleware.cors import CORSMiddleware

origins [
	...
]

app.add_middlewre(
	CORSMiddleware,
	allow_origins = origins,
	allow_credentials = True,
	allow_methods = ["*"];
	allow_methods = ["*"]
)

@app.get("/")
async def main():
	return {"message": "Hello world"}
```

## Arborescence recommandée
- app/
  - api/
  - authorization/
  - datasource/
  - domain/
  - setup/
  - static/
  - tests/
  - main.py
  - requirements.txt
- documentation/
- pipeline/
- .gitignore
- dev-requirements.txt
- README.md

## Séparer mes routes de l'API
Pour faire de l'inclusion conditionnelle (pour gérer les authorisations, ...)

```python
router = APIRouter(tags=[TAG])

@router.get("/")
async def about() -> Dict[str, str]:
	return {
		"app_version": VERSION,
		"api_url_doc": URL_DOC,
		"api_rul_swagger": URL_SWAGGER,
		"app_contacts": 
	}

app = FastAPI(
	title = NAME,
	version = VERSION,
	openapi_tags = tags_metadata,
	redoc_url = URL_DOC,
	docs_url = URL_SWAGGER,
	contact = CONTACTS
)

app.include_router(utils.router)
```