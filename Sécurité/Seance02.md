# Firewall
>[!Define] Firewall
>Filtre les connections entrantes et sortantes à partir de règles et d'exceptions.

Action: IP source -> IP dest: port

## Types
- Firewall stateless: contrôle tous les accès individuellement,
- Firewall statefull: garde trace des connections précédentes pour savoir quoi autoriser,
- Firewall NG (Next Generation): peut utiliser plus d'informations pour ses règles ne fonctionne que s'il est point de terminaison SSL,
- Firewall applicatif (= WAF).

>[!Define] URL Filtering
>On choisit de et vers quelle URL on envoie des requêtes (en s'abonnant à des listes d'url dangereuses ou non dangereuses).