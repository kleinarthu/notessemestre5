# Introduction au Web sémantique
## Caractéristiques du Web
>[!Define] Web
>Ensemble de pages reliées entre elles par des hypertextes qui mènenet vers des adresses web.
### Composants de l'architecture client/serveur
- Identification et adressage (**URL**),
- Communication / protocole (**HTTP**),
- Langage de représentation (**HTML**).

>[!Define] W3C
>**World Wide Web Consortium**: Harmonise l'évolution du Web et le standardise de façon à garantir l'interopérabilité.
### Moyens d'accès:
- Adresse Web,
- QR code.
=> Ne se limite plus au monde informatique.

>[!Define] URL
>Uniform Resource Locator: Identifie ce qui existe dans le web.

>[!Define] URI
>Uniform Resource Identifier: Identifie, sur le web, tout ce qui existe (même si ce n'est pas une page web).

>[!Define] IRI
>Internationalized Resource Identifier: Identify sur le web, dans n'importe quelle langue, ce qui existe.

## Web sémantique (WS)
### Web 2.0
- Pages purement syntaxique,
- Conçu pour être utilisé par des humains,
- Limite la recherche, l'extraction, la maintenance et la génération d'information.

=> Besoin d'ajouter une couche de connaissance au dessus du Web pour être utilisée par les ordinateurs uniquement. => **web sémantique**

### Définition
>[!Define] Web sémantique
>Prolongement du web qui ajoute une signification clairement définie aux informations pour permettre une plus étroite collaboration entre l'humain et la machine.

- Ensemble de connaissances (plutôt que de connaissances),
- Basé sur XML et RDF (plutôt que HTML),
- Basé sur les concepts (et non les mots-clés).
### Standard du WS
![](https://www.w3.org/DesignIssues/diagrams/sweb-stack/2006a.png)

## RDF: Resource Description Framework
>[!Define] RDF
>Standard du Web sémantique pour la description des ressources.

Repose sur les URI,
Ensemble de déclaration
>[!Define] Déclaration
>Sujet, Prédicat (propriété), Objet

>[!Example]
>index.html -> Creator -> Berners lee

=> **graph**

On représente les littéraux par des carrés dans le graphe.

### Name Spaces
Réduit le nom des ressources et prédicats utilisés en remplaçant un uri par un préfixe (eg local:name à la place de http://mon_site.fr/chemin/name)
#### Préfixes normalisés
- rdf,
- rdfs
- dc,
- owl,
- xsd,
### Format de sérialisation
![](https://www.w3.org/2013/dwbp/wiki/images/thumb/1/17/RDFSerialization-formats.png/800px-RDFSerialization-formats.png)
#### RDF/XML
```xml
<rdf:Description rdf:about="Sujet">
	<dc:predicat rdf:resource="Objet">
</rdf:Description>
```

#### Turtle
```turtle
@base <http://www.cat.com/docs> .
@prefix cs: <http://www.cat.com/schema/> .
@prefix dc: <http://purl.org/dc/elements/1.1/> .
R20301 a cs:PersonalDoc ;
	dc:creator auth:R051156 ;
	dc:title "Karin Hoepage" ;
	dc:date "2021-01-20"^^xsd:date .
```
## RDFS
>[!Define] RDFS
>RDF Schema s'écrit à l'aide de triplet RDF et spécifie les clases des ressources que vérifieront leurs instances

>[!Info] Documentation
>https://www.w3.org/TR/rdf12-schema/
# SPARQL
>[!Define] SPARQL
>**Simple Protocol And RDF Query Language**: Language et protocole de requête pour l'interrogation de méta-données RDF.

## Syntaxe
### Sélection
```sparql
PREFIX NameSpace
SELECT ?Variable
WHERE {
	modèle de requête (motif)
}
```

>[!Example]
> Tout extraire :
>```sparql
>SELECT ?subject ?predicate ?object 
>WHERE {
>	?subject ?predicate ?object .
>}
>```

### Limiter le nombre de sorties
```sparql
SELECT *
WHERE {
	?s ?p ?o.
}
LIMIT 20
```

### Filtrer
```sparql
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
SELECT *
WHERE {
	$s rdfs:label $o.
}
```

a remplace rdfs:type

### Comptage
```sparql
SELECT ?name count(?film) AS ?nbreFilms
WHERE {
	?producer dbp:name ?name
	?film a dbo:Film;
		dboproducer ?producer.
}
GROUP BY ?name
ORDER BY DESC(?nbreFilms)
```

### Filter
```sparql
SELECT ?name count(?film) AS ?nbreFilms
WHERE {
	?producer dbp:name ?name
	?film a dbo:Film;
		dboproducer ?producer.
FILTER (REGEX(?name, "Thomas.*"))
}
GROUP BY ?name
ORDER BY DESC(?nbreFilms)
```

### Contrainte optionnelle
```sparql
SELECT ?name ?quote
WHERE {
	?film a dbo:Film;
		dbp:name ?name;
		dboproducer dbr:James_Cameyron.
	OPTIONAL(?film dbr:quote ?quote)
FILTER (REGEX(?name, "Thomas.*"))
}
GROUP BY ?name
ORDER BY DESC(?nbreFilms)
```

### Combiner des tables
Suivre le bloc WHERE par un bloc UNION.

# Linked Open Data (LOD)
Application du web sémantique.
>[!Define] LOD
>Ensemble de jeux de données publiées sur le Web qui utilise les technologies du Web Sémantique pour lier les données de chaque autres graphes.

## Principes de base
- Identifier les choses avec des URI,
- Les URI doivent être compatibles avec le web du document (commencer par http),
- Fournir des renseignements lisibles par les humains et par les machines en utilisant RDF ou SPARQL,
- Les URI donnent accès à d'autres URI pour stimuler l'interconnexion.

## Mode d'accés
>[!Define] SPARQL Endpoint
> Utilise SPARQL pour accéder aux connaissances de LOD

>[!Example]
>- https://dbpedia.org/sparql

# Linked Open Vocabularies (LOV)
>[!Question] Quel vocabulaire utiliser pour uniformiser les propriétés?

=> LOV

>[!Define] LOV
>Catalogue de vocabulaires disponible pour être réutilisés dans le but de décrire des données sur le Web.
>Combine les ontologies utilisés par plusieurs bases de connaissances.

>[!Example]
>FOAF (friend of a friend project)
# Les ontologies

>[!Define] Ontologie
>- Partie de la métaphysique (philosophie) qui étudie le **type** des choses,
>- Spécification explicite d'une **conceptualisation**,
>- **Compréhension partagée** d'un domaine d'intérêt,
>- **Spécification** formelle et explicite d'une conceptualisation partagée.

## Buts
- représenter au mieux les connaissances pour les machines et les humains,
- partager ces connaissances,
- ré-utiliser les connaissances.

Des ontologies sont créées pour plusieurs **domaines** (langage, médecine, connaissances, ...).

## Types d'ontologies
>[!Define] Top-Ontologie
>Structure les connaissances de haut niveau à l'aide de termes très généraux.
> Trop général pour être instancié.

>[!Define] Core-Ontologie
> Fournit les concepts structurant le domaine.

>[!Define] Ontologies de domaine
>Fournit les concepts du domaine utilisé.

But:
- Retrouver des éléments communs entre des domaines distincts,
- Avoir une interopérabilité entre les différentes ontologies.

## Composants
- **Concepts** désigné par un nom qui peut être accompagné d'une valeur instanciée par le concept ou ses individus,
- **Relations** entre différents concepts (un domain et un range),
- **Axiome**: Contraintes sur les connaissances, relation entre concepts et relations (propriété obligatoires, cardinalité, transitivité, ...).

## OWL
>[!Define] OWL
>**Web Ontology Language**: repose sur RDFS.
- classes,
- héritage (subClassOf),
- instances,
- propriétés (ObjectProperty si la valeur est un individu, DataProperty si la valeur est un littéral),
- caractéristiques de ces propriétés.

### Outils ajoutés par rapport à RDF
- Disjonction,
- Combinaison booléennes de classes,
- Restrictions de cardinalité,
- Caractéristiques de ces propriétés,
- Étendue locale des propriétés, du range.

Avec RDF, rien n'empêche de dire:
"pomme fuji" a pour frère "Julien"

### Logiciel utilisé
[Protégé](https://protege.stanford.edu/)

>[!Define] Raisonneur
>Permet de conduire des raisonements et de dériver des conclusions à partir d'une base de connaissance pour vérifier sa cohérence et ajouter de nouveau faits.