# Projet
## Rendu
Rendu des logos le vendredi 19 janvier matin à 9h.
## Logiciels à utiliser
- Commencer par papier-crayon.
- Inkscape,
- Illustrator,
- Photoshop,
- Photoshop.
## Mise en forme
>[!Define] Moodboard
>Planche contenant les couleurs et typographies utilisées.

Par exemple avec inVision

# Caractérisation d'un logo
## Intérêt des logos
- Différentiation
- Mémorisable,
- Apporter des informations,
- Renforce l'image,
- Définit la charte graphique,
- Met en avant la marque, séduire (vêtements),
- Identifier l'activité,
- Fédérer les clients,
- Représenter le personnel.

## Qualités d'un bon logo
- Simple,
- Identifiable,
- Efficace,
- Reproduisable,
- Doit fonctionner à plusieurs tailles et en noir et blanc,
- Lisible,
- Intemporel,
- Fonctionnel et utilisable dans tous les contextes,
- Respecter les codes graphiques de son secteur d'activité.