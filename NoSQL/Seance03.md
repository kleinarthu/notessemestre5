# Neo4j
Orientée graphe

## Introduction
- Données de plus en plus **connectées**,
- Les bases de données relationnelles ont été envisagées mais les **jointures sont coûteuses**,
- Les autres bases de données NoSQL ne comportent pas de requêtes pour étudier les relations entre les objets,
-> Se baser sur les graphes pour analyser les données.

>[!Define] Graphe
>Ensemble de points reliés entre eux par des arcs.

## Concepts
>[!Define] Noeud
>Entité

>[!Define] Label
>Regroupe des noeuds par type.

>[!Define] Relation
>Lien entre deux noeuds

>[!Define] Propriété
>Caractéristique d'un noeud associée à un littéral

>[!Success] Avantages
>- Performance accrue,
>- Stockage optimisé pour des données de type graphe,
>- Traitement de graphe facile,
>- Flexibilité.

>[!Example]
>- Neo4j,
>- Neptune,
>- Infinite Graph,
>- AllegroGraph.

## Neo4j
>[!Define] Neo4j
>Logiciel libre développé en Java. C'est une des bases de données graphes les plus puissantes et robustes.

>[!Define] ETL
>Extract Transform Load
## Caractéristiques
- très utilisé par de grosses entreprises,
- schemaless,
- native (graphe pensées dès le début),
- Whiteboard Friendly,
- ACID (changements stockés jusqu'à leur fin, processus de récupération, isolation et tri des opérations, applique les changements dans les fichiers),
- simple à utiliser,
- performant,
- scalable,
- ETL (simplifie les imports),
- Drivers et API pour les langages standards,
- HA (High Availability),
- Réplication maître-esclave.

## Stockage des données
- Réplication,
- Plusieurs fichiers,
- Listes d'enregistrements,
- Noeuds, propriétés et relations enregistrées séparément,
- Sharding.

>[!Define] Neo4j Fabric
>Solution de Neo4j pour le sharding des graphes en permettant aux utilisateurs de décomposer le graphe. Cela impose beaucoup de redondance si les graphe est fortement connecté.

## Accés
[Neo4j Browser](https://neo4j.com/download/neo4j-desktop/?edition=desktop&flavour=unix&release=1.5.9&offline=true)
[Accéder via le navigateur](http://localhost:7474)
Utilise Bolt
>[!Define] Bolt
>Protocole de communication TCP plus performant que http.

## Cypher Query Language
>[!Define] Cypher
>Langage de requête de Neo4j.
### Notations
#### Noeuds
- Noeud anonyme: (),
- Noeud nommé: (x),
- Noeud d'un label: (:y),
#### Relations
- Relation anonyme: -->,
- Relation nommée: -\[r\]->,
- Relation nommée avec un label: -\[r:t\]->,
- Relation entre deux noeuds: (a)-\[r\]->(b),

#### Clauses
- MATCH,
- DISTINCT,
- OPTIONAL MATCH,
- WHERE,
- RETURN.

>[!Example]
>```Cypher
>Match (n) RETURN n
>
>MATCH (n: Product{name: "Neo4j in a nutshell"})
>RETURN n
>
>MATCH (n:Product)
>WHERE n.name = "Neo4j in a nutshell"
>RETURN n
>```

#### Clauses de création
- CREATE,
- MERGE (relations),
- SET,
- REMOVE,
- DELETE.

>[!Example]
>```Cypher
>CREATE (nico:Person{name: "Nicolas", from: "France"})
>RETURN nico;
>```
#### Fonctions utiles
- id(),
- labels(),
- type() (pour les relations),
- length(path), relationships(path), nodes(path),
- timestamp(),
- upper(),
- lower(),
- range(i0, if).

#### Fonctions aggregation
- SUM,
- AVG,
- COUNT,
- COLLECT.