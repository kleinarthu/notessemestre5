# JavaScript et NodeJS
## Historique
- Créé en 1995,
- Créé en 10 jours,
- Créé par Brendan Eich pour Netscape,
- Première utilisation sur navigateur en 1996,
- Devient un standard de développement frontend (ECMA).
## Caractéristiques
- Multi-paradigme,
- Interprété (la plupart du temps),
- Typage faible,
- Typage dynamique.
## Exécution
- Sur un navigateur généralement,
- Dans un terminal grâce à Node.
>[!Define] NPM
>**Node Package Manager**: Permet l'installation et la mise à jour de Node et de ses packages externes.

>[!Define] NVM
>**Node Version Manager**: Permet la cohabitation de plusieurs versions de node sur le système.

```shell
npm install npm@latest -g
nvm install node
```

## Fonctions
### Affichage
```js
console.log("Hello World!");
```

### Déclaration de variable
```js
let variable_locale = 0;
var variable_plus_globale_pas_toujouts_initialisée = 0;
const constante_globale_sur_le_projet = "STATIC VARIABLE";
```

### Types primitifs
- **number**,
- **string**,
- **boolean**.
```js
typeof ma_variable;
'string'
```
Beaucoup de fonctions existent déjà pour leur manipulation

### Pointeurs nuls
- **undefined** (explicitement ou implicitement si var sans valeur initiale),
- **null**.

### Fonction
```js
function nom_de_ma_fonction(parametre) {
	return 'Hello World';
}
```

### JSON
>[!Define] JSON
>JavaScript Object Notation

```js
let my_object = {
	key_1: 'value_1',
	key_2: 3
}
```

### Création de classe

```js
class Book {
	constructor(parametre) {
		this.attribut = parametre;
	}
}
```

### Tableaux
```js
let numbers = [1, 3, 4];
numbers[0];
// Si on cherche en dehors du tableau, on reçoit undefined.
numbers.length
numbers.push(8);
numbers.unshift(0); // push au début
numbers.pop();
```

### Set
```js
const set = new Set([1, 2, 3]);
set.has(1); // true
```

### Map
```js
const my_map = new Map();
my_map.set(key, value);
my_map.size;
my_map.get(key);
```

### Conditions
- == pour la valeur,
- === pour la valeur et le type,
- &&,
- ||,
- !.
Undefined et null sont évalués à false.

### Switch
```js
switch (variable) {
	case value1:
		break;
	default:
}
```

### Boucles
```js
for (let i = 0; i < 5; i ++) {
}
for (let val in liste) {
}
for (let val of liste) {
}
while (condition) {
}
do {
} while (condition)

```

### Exceptions
```js
try {
} catch {
}
```

### Lambdas
```js
const ma_fonction = (parametre) => {
};
```

## Classes avancées
### Ajouter méthode
```js
car.start = (var) => ({return 2};
```

### Encapsulation
```js
_var_privee_par_convention = 0;
```

### Getters et setters
Les getters et setters sont automatiquement appelés

### Héritage
```js
class Fille extends Mere {
}
```

### Prototypage
~ Décorateur

```js
String.prototype.nom_prototype = nouvelle_fonction_pour_toutes_String;
```

Préférer surcharger la classe pour rendre le prototype plus explicite.

### Object to string
```js
let json = JSON.stringify(my_obj);
let obj = JSON.parse(my_json);
```

## Outils fonctionnels

### Parcours
```js
my_array.forEach((current_value) => {do_something()});
```

### Map
```js
my_array.map((current_value) => {return new_value;});
```

### Filter
```js
my_array.filter((current_value) => {return should_stay;})
```

### Reduce
```js
my_array.reduce((temp_res, current_value) => {return new_res;})
```

### Sort
```js
my_array.sort((one_value, an_other_one) => {an_other_one - one_value});
```

## Promesses
>[!Define] Promesse
>Résultat, mais on ne sait pas quand il sera disponible.

```js
let promise = new Promise(function(resolve, reject) {
	setTimeOut(() => resolve(1), 1000);
}).then(function(result) {
	return do_something_when_available(result); // Still a promise
}).then(function(result) {
	return do_something_when_available(result);
}); // Still a promise
```

```js
async function my_func() {
	let value = 0;
	let promise = new Promise(function_that_takes_time)
	.then(do_something(result));
	await promise;
	return value;
}
```

## Communication avec les api
```js
const axios = require('axios');
axios.get("http://api.icndb.com/jokes/random")
	.then(response => console.log(response.data))
	.catch(error => console.log(error));
```