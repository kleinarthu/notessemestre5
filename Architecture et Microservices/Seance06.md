# Projet
## Objectif
Permettre à des groupes de personnes de regarder un film (Netflix sans streaming et avec des propositions).

>[!Quote]
>Recommander, un ou plusieurs films, qui n'ont pas encore été vus par ce groupe de N personnes et qui partage un maximum de points commun entre ces personnes.
## Contexte
- On ne sait pas quoi regarder,
- On n'arrive pas à se mettre d'accord.
## Date limite
2 avril
## Outils
- Base de données de film: tmdb (https://www.themoviedb.org/?language=fr).
## Discussion autour de l'architecture
### Contraintes
- Contient FastAPI,
- Utilise tmdb.

## Proposition
![[Pasted image 20240110103048.png]]

### Technos
- Frontend: React,
- Identity provider: OAuth,
- Recommendation: Scikit-learn,
- User Management: Go,
- DB: MariaDB.


### Fontend
- CGU,
- Contact,
- Présentation du site,
- Création de compte / Authentification,
- Recherche de films:
    - Accès à des pages de présentation de film,
    - Labélisation de films (Vu / Non Vu).
- Gestion de la préférence utilisateur (Genre de films préférés, langue de l'utilisateur),
- Gestion de groupes entre utilisateurs (CRUD).
- Génération de la recommandation pour un groupe donné.

## Backend
- Gére l'authentification et les token d'authentifiacation (JWT),
- Gère les groupes d'utilisateurs,
- Gère les préférences utilisateur,

## SQL Database
- Devra stocker des logs applicatifs,
- Gère les données utilisées par le Backend:
  - Si les données ne sont pas présentes en base, appeler l'API IMDB
   - 
## Contraintes techniques
- Log applicatif pertinent,
- Docker compose,
- Constantes dans un fichier de conf séparé.