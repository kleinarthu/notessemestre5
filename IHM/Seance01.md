# Horaires
- matin: 9h00-12h30,
- après-midi: 9h30-17h00.

# Objet de la matière
- Ergonomie,
- Expérience utilisateur.

# Évaluation
Port-folio.

# Partie théorique

>[!Define] Ergonomie  
>- Étude scientifique des relation de travail et relation entre homme et machine.
>- Adaptation d'un environnmenet de travail aux besoins de l'utilisateur.

-> Transformer les outils et non les personnes.

>[!Define] Utilisabilité  
>Aptitude d'un objet ou service à etre utilisé facilement par une personne.

## Critères de l'utilisabilité
- Efficience (atteindre l'objectif),
- Satisfaction,
- Facilité d'apprentissage,
- Facilité d'appropriation,
- Fiabilité.
## Vidéo: https://www.youtube.com/watch?v=rZl8cClHj9o
- **Utilisabilité**,
- améliorer la façon dont les gens vont utiliser nos outils et interfaces.

>[!Example]  
>- Évier britannique,
>- Évier européen.

>[!Define] Utilité  
>Capacité à servir à une activité.

>[!Define] Utilisabilité  
>Facilité d'utilisation

## Expérience utilisateur
>[!Example] Amazon  
>1. Avant: boites avec beaucoup de boutons pour créer un compte, payer, ...
>2. Redesign,
>3. 300_000_000$ de profit.

>[!Define] Expérienc eutilisateur (UX)  
>Qualité de l'expérience vécue par l'utilisateur lors de l'utilisation.

=> Plusieurs lois pour quantifier l'UX

## Loi de l'UX

### Loi de Fitts

>[!Define] Loi de Fitts  
>Temps nécessaire pour exécuter une tâche.
>-> Plus l'objet est grand et proche plus l'action est rapide.

>[!Example]  
>- souris: Aller moins loin vers des zone plus grande,
>- portable: Plus proche du pouce.

### Loi de Hick
>[!Define] Loi de Hick  
>Relation entre nombre de choix disponible et temps moyen.

Forme de la courbe: $RT = a + b \cdot log_2(n)$

### Théorie de la Gestalt
>[!Define] Théorie de la Gestalt  
>Le tout est différent de ses parties, notre cerveau donne du sens à des formes qui n'en ont pas initialement.

#### Loi de la bonne forme
Les formes reconnues sont simples, symétriques, stables, ...

#### Loi de la continuité
Le cerveau continue les formes, les prolonge.

#### Loi de la proximité
Le cerveau regroupe les points proches.

#### Loi de la similitude
Le cerveau regroupe les points similaires.

#### Loi du destin commun
Le cerveau regroupe les éléments qui suivent une même trajectoire.

#### Loi de la familiarité
Le cerveau reconnaît les objets familiers.

### Loi de Miller
>[!Define] Loi de Miller  
>Une personne peut mémoriser entre 5 et 9 objets (en fonction de l'environnement, du calme, ...).

>[!Example] Apple  
>Pas plus de 5 items pour valider une nouvelle application.

## Interface utilisateur
>[!Define] Interface utilisateur  
>Interface de communication entre la pensée humaine et la logique de l'application.

>[!Exemple] Mauvaises pratiques  
>- Trop de mises en valeur différentes,
>- Contenu poussé par pubs, menus, ...
>- Trop de place occupé par les menus -> trop défiler pour le vrai contenu,
>- Fenêtres pop,
>- Pubs au milieu du contenu.

### Règles pour construire une interface cohérente
1. Architecutre,
2. Organisation visuelle,
3. Cohérence,
4. Convention,
5. Information,
6. Compréhensino,
7. Assistance,
8. Gestion des erreurs,
9. Rapidité,
10. Liberté,
11. Accessibilité,
12. Satisfaction.

### Les couleurs
- 3 couleurs primaires (différentes en additive et substractive),
- Caractérisées par **teinte**, **saturation** et **luminosité**,
- Possèdent une symbolique,
- Vues différemment par les daltoniens.
-> Importance des contrastes.

#### Jaune
- gaieté,
- énergie,
- inspiration,
- mémoire,
- attention,
- appétit
- jeunesse,
- ...

#### Bleu
- paix,
- tranquilité,
- productivité,
- hommes,
- sécurité,
- technologies,
- ...

#### Rouge
- appétit,
- passion,
- amour,
- attention,
- ...

#### Vert
- écologie,
- nature,
- soin,
- accroissement,
- ...

#### Orange
- enthousiasme,
- générosité,
- mise en garde,
- appel à l'action,
- ...

#### Violet
- noblesse,
- richesse,
- magie,
- calme,
- sagesse

### Typographie
>[!Define] Police  
>Positionne les traits dans l'écriture

>[!Define] Fonte  
>Police où les traits ont une certaine largeur.

#### Sans Serif
- Conséquence du modernisme,
- plus nettes,
- plus fortes,
- plus contemporaines,
- plus froides,
- plus lisibles.

#### Cursive
- manuscrite,
- personnelles,
- rapidité.

#### Graphiquement construites
- géométriques,
- créatives,
- singulières (logo).

## Composition d'une image
>[!Define] Lignes de force  
>Lignes de force dirigent l’œil vers élément important.

## Symbolique des formes
### Cercle
- naturel,
- cycle de la vie, des saisons,
-> unité.

### Arc de cercle
- ciel,
- foi.

###  Carré
- rare dans la nature,
- ordre,
- raison,
- construction humaine.

### Rectangle
$\phi$ -> harmonie

### Triangle
- Direction,
- Focalise l'attention,
- Autorité,
- Flèches.

### Croix
- Intersection,
- Orientation,
- Silhouette humaine,
- Christianisme.

### Étoile
- Éternité,
- Espoir,
- Énergie,
- Quête de la vie.

### Losange
- Raffinement,
- Qualité,
- Luxe.

### Hexagone
- Structures sociales.

### Spirale
- Infinité,
- Mystère,
- Sérennité.

### Bouclier
- Sécurité,
- Protection,
- Autorité.

## Logotype
Standardisation des logos -> les typos deviennent très proches.

# Outils pour le rendu
## Carte mentale
- XMIND,
- Framasoft -> Framindmap.

## Présentation
- Power point,
- PITCH.APP.

## Réalisation
- papier et crayon,
- Illustrator.
