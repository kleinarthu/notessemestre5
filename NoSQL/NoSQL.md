# Base de donnée relationnelles
- Plus utilisées,
- Bien **structurées**,
- Données **cohérentes** (clés étrangères),
- **Formes normales** pour éviter la redondance,
- Accessible via **SQL**,
- **Centralisées** pour garantir la cohérence des données,
- Utilise l'approche **ACID**.

>[!Define] ACID  
>- **Atomicity**: Tout changement doit être effectué jusqu'au bout,
>- **Consistency**: Les transactions ne modifient pas l'intégrité de la base de données,
>- **Isolation**: Les opérations sont bien ordonnées et ne s'impactent pas les unes les autres,
>- **Durability**: Tous les changements effectués sont permanents.

### Inconvénients de ce système
- Difficulté de **mise à l'échelle** (une seule machine),
- Paradigm "One size fits all" non adapté aux **systèmes distribués**.
# NoSQL
>[!Define] NoSQL  
>Not only SQL.
>- Non-relationnelles,
>- Scalable horizontalement,
>- Réblicable.

>[!Failure] Inconvénient  
>ACID ne marche plus.

>[!Define] BASE  
>- **Basically Available**: Les données doivent être disponibles la plupart du temps,
>- **Soft state**: La base de donnée ne doit pas être cohérente à tous moments,
>- **Eventual consistency**: À terme la base doit retrouver la cohérence.

>[!Define] CAP Theorem  
>Il n'est pas possible de respecter les trois contraintes suivantes dans un système distribué:
>- **Coherency**,
>- **Availability**,
>- **Partition tolerance**.

=> Permet de classer toutes les bases de données dans le triangle CAP.

![](https://www.mysoftkey.com/wp-content/uploads/2016/09/cap-theorem-triangle.png)

>[!Success] Avantages  
>- Très **performantes**,
>- Utilise plusieurs niveaux de **structure**,
>- Facilement **expansible**,
>- Pas besoin de **schéma** bien défini,
>- N'utilise pas de **concept complexes** comme les jointures.

>[!Failure] Inconvénients  
>- Manque de **cohérence**,
>- Pas adaptés aux **payements**,
>- Pas de **compatibilité** entre NoSQL et SQL,
>- Pas de **standardisation**.

## Taxonomy
### Orientée Clé/Valeur
- Chaque ligne est un couple clé/valeur,
- Similaire à une map,
- Utilise les commandes : créer(clé, valeur), lire(clé), maj(clé, valeur), delete(clé).

>[!Example]  
>- Redis,
>- Amazon dynamo,
>- project Voldemort,
>- ...
### Orientée colonnes
- Organisées par colonnes seules plutôt que par tableau entier,
- Permet de citer dès le départ les colonnes où on va chercher de l'information.

>[!Example]  
>- Google BigTable,
>- Cassandra,
>- Amazon SimpleDB.
### Orientée graph
- Modélise des liens plutôt que des entités,
- Les opérations utilisent les algorithmes de graphes.

>[!Example]  
>- Neo4j,
>- graphDB.
### Orientée document
- Clé/valeur mais la valeur est un  document,
- Utilise JSON ou XML.

>[!Example]  
>- CouchDB,
>- MongoDB,
>- ...
# MongoDB
>[!Define] MongoDB  
>- Créé en 2007,
>- Orienté document,
>- Distribuable,
>- Réplicable.

>[!Info] Documentation  
>https://www.mongodb.com/docs/

>[!Define] Réplication  
>Les mêmes données sont copiées sur plusieurs hôtes.

>[!Define] Sharding  
>Distribue les données sur plusieurs machines pour éviter les pertes en cas d'échec (sans redondance).

>[!Success] Réplication  
>- Réduire la latence,
>- Augmente la disponibilité,
>- Diminue l'impact des erreurs.

## Types de réplication
- **Single-leader replication**: Le leader reçoivent et envoient les mises à jours à tous les replicas,
- **Multi-leader replication**: Les transmissions se font en suivant une topology (circulaire, en étoile, all to all, ...),
- **Leaderless replication**: Le client est responsable d'envoyer les écritures à tous les replicas.
## Leader-based replication
- Le leader reçoit une demande d'écriture,
- Il applique la modification dans ses données locales,
- Le leader informe tous les followers des modifications effectuées et envoie un replication log,
- Les followers effectuent les changement du log dans leur ordre.
### Réplication synchrone
Le leader attend que chaque follower ait fait la modification pour confirmer au client que la modification ait été faites.
>[!Success] Avantages  
>On est sûr que toutes les données soient identiques.

>[!Failure] Inconvénients  
>Le leader est bloqué pendant plus longtemps.
>Si un nœud est en panne, tout le système est bloqué.
### Réplication asynchrone
Le leader n'attend pas la confirmation des followers et reste actif.
### Ajout de nouveaux followers (catch-up recovery)
1. On réalise une snapshot de la base de donnée,
2. Le snapshot est transféré dans le nouveau replica,
3. Le follower demande les modifications effectuées depuis la snapshot,
4. Le follower est à jour.
### Récupération en cas de panne d'un replica
1. Chaque replica contient un log de toutes les modifications effectuées depuis le début,
2. Le follower compare son log à celui du leader,
3. Le follower traite la différence.
### Récupération en cas de panne du leader
1. Une nouvelle machine va être désignée comme leader (replica qui a le plus de mises à jour généralement),
2. Reconfiguration des clients pour envoyer les données au bon leader,
3. Les followers reçoivent les données du nouveau leader,
4. L'ancien leader est ensuite géré pour devenir follower.
## Caractéristiques de MongoDB
- Pas de jointure,
- Document limité à 16MB,
- Fournis des requêtes complexes,
- Format de document adaptés au web,
- Schemaless (pas besoin de schéma au départ),
- Les tables sont remplacées par des collections,
- Les lignes sont remplacées par des BSON (JSON binaire) documents,
- Les attributs sont remplacés par des champs.
## Principaux composants
- **mongod**: server,
- **mongo***: partie client.
## Commandes
### Sélection
```mongo
use dbname
```
### Création d'une collection
```mongo
db.collec.insert(data)
```
### Insertion
```mogo
var mov = {title: "Jaws", year: 1974};
db.movie.insert(mov);
db.movies.insertMany([mov1, mov2]);
```
### Mise à jour
```mongo
db.movies.update({title: "Les dents de la mer"}, {title: "Jaws"});
db.movies.update({title: "Les dents de la mer"}, $set, {title: "Jaws"});
```
Arguments: filtre, modification
$set permet de garder tous les autres champs
upsert = update if existe else insert

### Sélection
```mongo
db.movies.find({year: 1975}); // filtre
db.movies.find({year: 1975}, {title: 1, category: 1}); // Projection
db.movies.find({year: 1975}, {title: 0}); // Rejection
db.movies.find().sort({year: -1}); // Tris (ici descendant)
db.movies.find().skip(10).limit(10);
db.movies.count({year:{$gte: 2000}});
```
#### Autres opérateurs
- $lt,
- $le,
- $gt,
- $ge,
- $eq,
- $ne,
- $in,
- $ni,
- $or,
- $and,
- $not,
- $nor,
- $exists,
- $regex.
$where pour utiliser une fonction javascript.
### Agrégation
aggreagate pour créer un pipeline d'opérations.
- $count,
- $sum,
- $mean.

$lookup ~ left join
```mongo
{
	$lookup: {
		from:<collection>,
		localField: <field>,
		foreignField: <field>,
		as: <output array field>
	}
}
```

# Sharding
>[!Define] Sharding  
>Répartir/diviser la base de donnée entre les serveurs (ou **shard**) pour améliorer la performance du système.

Chaque shard doit être répliquée.

>[!Define] Mongos  
>Routeur de MongoDB qui redirige les différentes requêtes vers les différents shard.

Il est possible d'avoir plusieurs routeurs Mongos pour améliorer les performances et la disponibilité.

>[!Question] Sur quoi se base Mongos pour la répartition?  

- Les métadonnées des différents shards (eg titre de a-c).

Stockés dans les config (server).

>[!Define] Config (server)  
>Contient les métadonnées et les commandes de transfert des shards.

Les données ne sont pas directement stockés dans les shards mais sont stockés sous forme de chunks.

>[!Define] Chunk  
>Bloc contenant une partie des données.

Taille défaut: 128 MB par chunk

Mongodb utilise la clé de sharding pour créer les chunks.

## Méthodes de Sharding

### Range Based Sharding
>[!Define] Range Based Sharding  
>Répartit les chunks selon la valeur d'un champs (un chunk correspond aux données qui ont, pour cette clé, une valeur dans un certain intervalle).

>[!success] Avantages  
>Facile de rediriger le travail pour Mongos.

>[!failure] Inconvénients  
>Peut donner une distribution des données non-équitable.

### Hash Based Sharding
>[!Define] Hash Based Sharding  
>Utilise une fonction de hashage à partir d'un champ pour placer la donnée.

>[!success] Avantages  
>Permet une distribution aléatoire automatiquement aléatoire des données.

>[!failure] Inconvénients  
>Mongos ne peut pas diriger aussi facilement les requêtes.

# Neo4j
Orientée graphe

## Introduction
- Données de plus en plus **connectées**,
- Les bases de données relationnelles ont été envisagées mais les **jointures sont coûteuses**,
- Les autres bases de données NoSQL ne comportent pas de requêtes pour étudier les relations entre les objets,
-> Se baser sur les graphes pour analyser les données.

>[!Define] Graphe  
>Ensemble de points reliés entre eux par des arcs.

## Concepts
>[!Define] Noeud  
>Entité

>[!Define] Label  
>Regroupe des noeuds par type.

>[!Define] Relation  
>Lien entre deux noeuds

>[!Define] Propriété  
>Caractéristique d'un noeud associée à un littéral

>[!Success] Avantages  
>- Performance accrue,
>- Stockage optimisé pour des données de type graphe,
>- Traitement de graphe facile,
>- Flexibilité.

>[!Example]  
>- Neo4j,
>- Neptune,
>- Infinite Graph,
>- AllegroGraph.

## Neo4j
>[!Define] Neo4j  
>Logiciel libre développé en Java. C'est une des bases de données graphes les plus puissantes et robustes.

>[!Define] ETL  
>Extract Transform Load
## Caractéristiques
- très utilisé par de grosses entreprises,
- schemaless,
- native (graphe pensées dès le début),
- Whiteboard Friendly,
- ACID (changements stockés jusqu'à leur fin, processus de récupération, isolation et tri des opérations, applique les changements dans les fichiers),
- simple à utiliser,
- performant,
- scalable,
- ETL (simplifie les imports),
- Drivers et API pour les langages standards,
- HA (High Availability),
- Réplication maître-esclave.

## Stockage des données
- Réplication,
- Plusieurs fichiers,
- Listes d'enregistrements,
- Noeuds, propriétés et relations enregistrées séparément,
- Sharding.

>[!Define] Neo4j Fabric  
>Solution de Neo4j pour le sharding des graphes en permettant aux utilisateurs de décomposer le graphe. Cela impose beaucoup de redondance si les graphe est fortement connecté.

## Accés
[Neo4j Browser](https://neo4j.com/download/neo4j-desktop/?edition=desktop&flavour=unix&release=1.5.9&offline=true)
[Accéder via le navigateur](http://localhost:7474)
Utilise Bolt
>[!Define] Bolt  
>Protocole de communication TCP plus performant que http.

## Cypher Query Language
>[!Define] Cypher  
>Langage de requête de Neo4j.
### Notations
#### Noeuds
- Noeud anonyme: (),
- Noeud nommé: (x),
- Noeud d'un label: (:y),
#### Relations
- Relation anonyme: -->,
- Relation nommée: -\[r\]->,
- Relation nommée avec un label: -\[r:t\]->,
- Relation entre deux noeuds: (a)-\[r\]->(b),

#### Clauses
- MATCH,
- DISTINCT,
- OPTIONAL MATCH,
- WHERE,
- RETURN.

>[!Example]  
>```Cypher
>Match (n) RETURN n
>
>MATCH (n: Product{name: "Neo4j in a nutshell"})
>RETURN n
>
>MATCH (n:Product)
>WHERE n.name = "Neo4j in a nutshell"
>RETURN n
>```

#### Clauses de création
- CREATE,
- MERGE (relations),
- SET,
- REMOVE,
- DELETE.

>[!Example]  
>```Cypher
>CREATE (nico:Person{name: "Nicolas", from: "France"})
>RETURN nico;
>```
#### Fonctions utiles
- id(),
- labels(),
- type() (pour les relations),
- length(path), relationships(path), nodes(path),
- timestamp(),
- upper(),
- lower(),
- range(i0, if).

#### Fonctions aggregation
- SUM,
- AVG,
- COUNT,
- COLLECT.
