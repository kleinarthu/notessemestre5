En lien avec la charte éthique de l'IESF.

# Risques professionnels
## Composantes
- Danger,
- Risque,
- Dommage.

## Types
- Risques de trébuchements, heurt ou autre perturbation du mouvement,
- Risques de chutes de hauteur,
- Risques liés aux circulations internes de véhicules,
- Risques routiers en mission,
- Risques liés à la charges physique de travail,
- Risques liés à la manutention mécanique,
- Risques liés aux produits et émissions de déchets
- Risques liés aux agents biologiques,
- Risques liés aux équipements de travail,
- Risques liés aux effondrements et aux chutes d'objets,
- Risques et nuisances liés aux bruit,
- Risques liés aux ambiances thermiques,
- Risques d'incendie, d'explosion,
- Risques liés à l'électricité,
- Risques liés aux ambiances lumineuses,
- Risques liés aux rayonnements,
- Risques psychosociaux.

## Pourquoi les prévenir?
- 2 à 3% du PIB,
- Augmentation de 27.1% depuis 2012,
- Obligation pour l'employeur,
- Pour tout le monde.

## Comment
- Culture de prévention au sein de l'entreprise,
- Évaluer les risques,
- Mieux concevoir les lieux de travail,
- Faire attention aux machines,
- Questionner l'utilisation des outils,
- Intégrer des services de santé et sécurité,
- Utiliser des EPI,
- Former le personnel,
- ...


>[!Define] EPI
>**Équipement de Protection Individuel**: Casque, lunettes, chaussure de sécurité, ...

## Acteurs concernés
- Ressources internes: dirigeants, salariés, IRP, médecine du travail,
- Ressources externes: inspection du travail, ANACT, INRS, IPRP

>[!Define] IRP
>Instance Représentative du travail

>[!Define] ANACT
>Agence Nationale de Recherche et de Sécurité

>[!Define] INRS
>Institut National de Recherche et de Sécurité

>[!Defein] IPRP
>Intervenant en Prévention Des Risques Professionnels

-> DUER

>[!Define] DUER
>**Document Unique d'Évaluation des Risques**

## Définition
>[!Define] Risques psychosociaux
>Situations de travail où sont présents, combinés ou non:
>- du stress,
>- des violences internes à l'entreprise,
>- des violences externes,
>- le syndrome d'épuisement professionnel.

## Stress
>[!Define] Stress professionnel
>Un état de stress survient lorsqu'il y a un déséquilibre entre la perception qu'une personne a des contraintes que lui impose son environnement et la perception qu'elle a de ses propres ressources pour y faire face.

### Formes du stress (Henri Laborit)
- Stress Psychologique,
- Stress Psychosocial.

- Fuite,
- Lutte,
- Inhibition.

## Burn out
### Étapes
- Lunes de miel: On est à fond dans ce qu'on fait,
- Phase de résistance,
- Rupture.

### Causes
- Surcharge de travail et complexité des tâches,
- Abscense de contrôle et non maîtrise d ses activités,
- Manque de récompense et de reconnaissance pour les efforts fournis: Travailler plus et ne pas gagner plus,
- Décalage entre les valeurs de l'individu et celles de l'entreprise,
- ...

## Solutions
- Mieux évaluer la charge de travail,
- Donnez de l'autonomie aux salariés,
- Soutenez les collaborateurs,
- Témoignez de la reconnaissance,
- Donnez du sens au travail,
- Agir face aux agressions externes,
- Communiquer sur les changements,
- Faciliter la conciliation travail et vie privée,
- Bannir toute forme de violence.