# TDD
## Définition
>[!Define] TDD
>Écrire les tests en premiers et écrire le code minimal qui le complète.

## Cercle du TDD
Test fails -> Test passes -> Refactor

## Librairies
- Pytest pour Python
- Junit pour Java
- Jes pout JS/TS,
- Plunit pour Prolog.

# Cloud
Permet de ne plus s'occuper soi-même de ses serveurs.

>[!Example] Externalisation des données
>- Dropbox,
>- iCloud,
>- Google Drive.

>[!Example] Externalisation des application
>- Google Doc,
>- Office 365.

>[!Define] Cluster
>Ensemble de serveurs qui fonctionnent ensemble.

>[!Define] Scale up
>Augmenter les performance, la capacité d'une application.

>[!Define] Scale out
>Dupliquer la machine pour répartir la charge.

>[!Define] SaaS (Software as a Service)
>Logiciel et application disponibles online (Netflix, Twitter, ...), payé par abonnement.

>[!Define] **IaaS** (Infrastructure as a Service)
> Gestion distante de l'infrastructure applicative permettant d'utiliser l'IaaC (Infrastructure as a Code).
> La gestion du ressource est effectuée par le provider.

>[!Define] PaaS (Platform as a Service)
>Plateforme permettant le monitoring des ressources (Azure Portal, AWS Platform).
>Intermédiaire pour créer l'IAAS.

Marché à 150 Mrd USD au Q1 2021.
L'IaaS est le centre d'intérêt des DevOps.
Utilisation de **Terraform**.

## Terraform
>[!Define] Terraform
>Fournit un même interface quel que soit le provider.

>[!Success] Avantages
>Facilite les changements de provider.

Langage Turing complet (util si besoin de générer des password, de faire du changement de caractères, etc...).

### Fonctionnement
1. Identifier les besoins,
2. Identifier les services associés,
3. Séparer la mise en place de ces services pour éviter le redéploiement complet.

1. Init,
2. Find,
3. Plan,
4. Apply.

### Layer applicatif
1. Trouve les ressources à l'intérieur du catalogue du Cloud Provider,
2. Regarde l'état précédent de l'infrastructure et planifie le changement (Vérifie que rien n'ai été créé manuellement),
3. Applique le changement sur les ressources déclarées dans le main.tf.

>[!Important]
>Ne rien créer à la main.

>[!Define] ETL
>Extract, Transform, and Load.

### Organisation du code
Dossier config:
- config.tf,
- main.tf (définit toute les ressources et le provider avec sa version),
- variables.tf (types et noms des variables des ressources),
- prod.tfvars (variables de production).