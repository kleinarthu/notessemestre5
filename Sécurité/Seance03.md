# Vérifier la sécurité réseau
>[!Info]
>[pingcastle](https://www.pingcastle.com/)(ne pas installer sans autorisation)
# Règles d'hygiène de l'ANSSI
Pour beaucoup de règles, propose une norme renforcée pour se rapprocher de l'ISO27001.
1. Former les équipes opérationnelles à la sécurité des systèmes d'information,
2. Sensibiliser les utilisateurs aux bonnes pratiques élémentaires de sécurité informatique,
3. Maîtriser les risques de l'infogérance,
>[!Define] Infogérance
>Donner à une autre entreprise la gestion de votre système.

4. Identifier les informations et serveurs les plus sensibles et maintenir un schéma du réseau,
>[!Define] SNMP
>Protocole de communication qui questionne une MIB alimenté par tous les producteurs de logiciels pour avoir toutes les informations sur les logiciels.

>[!Define] MIB
>Management Information Base: Ensemble de toutes les informations disponibles pour une entité réseau.

5. Disposer d'un inventaire exhaustif des comptes privilégiés et le maintenir à jour,

>[!Important]
>Mettre en place une nomenclature pour identifier pour chaque compte son propriétaire et son rôle (eg ADupont_admin).

6. Organiser les procédures d'arrivée, de départ et de changement de fonction des utilisateurs,
7. Autoriser la connexion au réseau de l'entité aux seuls équipements maîtrisés,
8. Identifier nommément chaque personne accédant au système et distinguer les rôles utilisateur/administrateur,
9. Attribuer les bons droits sur les ressources sensibles du système d'information,
10. Définir et vérifier des règles de choix et de dimensionnement des mots de passe,
>[!Important] Gestionnaire de mots de passe
>Les utilisateurs n'ont plus besoin de connaître leurs mots de passes (moins de mots de passes simples, moins de mots de passes communs).
>>[!Example]
>>[Keepass](https://keepass.info/)

11. Protéger les mots de passe stockés sur les systèmes,
12. Changer les éléments d'authentification par défaut sur les équipements et services,
13. Privilégier lorsque c'est possible un authentification forte,
14. Mettre en place un niveau de sécurité minimal sur l'ensemble du parc informatique,
15. Se protéger des menaces relatives à l'utilisation de supports amovibles,
16. Utiliser un outil de gestion centralisée afin d'homogénéiser les politiques de sécurité,
17. Activer et configurer le pare-feu local des postes de travail,
18. Chiffrer les données sensibles transmises par voie Internet,
19. Segmenter le réseau et mettre en place un cloisonnement entre ces zones,
20. S'assurer de la sécurité des réseaux d'accès Wi-Fi et de la séparation des usages,
21. Utiliser des protocoles réseaux sécurisés qu'ils existent,
22. Mettre en place une passerelle d'accès sécurisé à Internet,
23. Cloisonner les services visibles depuis Internet du reste du système d'information,
24. Protéger sa messagerie professionnelle,
25. Sécuriser les interconnexions réseau dédiées avec les partenaires,
26. Contrôler et protéger l'accès aux salles serveurs et aux locaux techniques,
27. Interdire l'accès à Internet depuis les postes ou serveurs utilisés pour l'administration du système d'information,
28. Utiliser un réseau dédié et cloisonné pour l'administration du système d'information,
29. Limiter au strict besoin opérationnel les droits d'administration sur les postes de travail,
30. Prendre des mesures de sécurisation physique des terminaux nomades,
31. Chiffrer les données sensibles, en particulier sur le matériel potentiellement perdable,
32. Sécuriser la connexion réseau des postes utilisés en situation de nomadisme,
33. Adopter des politiques de sécurité dédiées aux terminaux mobiles,
34. Définir une politique de mise à jour des composants du système d'information,
35. Anticiper la fin de la maintenance des logiciels et systèmes et limiter les adhérences logicielles,
36. Activer et configurer les journaux des composants les plus importants,
37. Définir et appliquer une politique de sauvegarde des composants critiques,
38. Procéder à des contrôles et audits de sécurité réguliers puis appliquer les actions correctives associées,
39. Désigner un référent en sécurité des systèmes d'information et le faire connaître auprès du personnel,
40. Définir une procédure de gestion des incidents de sécurité,
41. Mener une analyse de risques formelle,
42. Privilégier l'usage de produits et de services qualifiés par l'ANSSI.

>[!Define] APT
>Application Persistent Threat: Faille exploitée sans connaissance depuis très longtemps.

## Analyse de risques
Méthodes:
- EBIOS,
- ISO27005.

# Proxy
## Forward Proxy
On met en entrée tous les utilisateurs qui entrent dans le proxy pour masquer les addresses de réseau interne et automatiser la bande passante (les pages que tous le monde charge ne seront téléchargées qu'une seule fois).

>[!Define] Proxy transparent
>Non déclaré chez le client (souvent intégré au FireWall).

>[!Example]
>- Squid,
>- Olfeo,
>- Ucopia.
## Reverse Proxy
On met en sortie tous les serveurs web pour réécrire l'URL (vérifie que l'URL soit légitime et redirige).

>[!Example]
>- HAProxy,
>- Apache,
>- Nginx,
>- Squid.

# Load Balancing
- Répartit les charges,
- Optimise la fiabilité.

# ssh
## Tunnel ssh
Possibilité de faire du port forwarding pour accéder à internet depuis un post qui n'est pas censé y avoir accès.
## Copier fichiers
- scp,
- rsync.