# Tp d'aujourd'hui
- Tp collectif,
- Objectif: que ça marche,
- Travailler ensemble -> créer une base de donnée distribuée avec toutes les machines,
- Voir tous les cas avec toutes les machines.
# Sharding
>[!Define] Sharding
>Répartir/diviser la base de donnée entre les serveurs (ou **shard**) pour améliorer la performance du système.

Chaque shard doit être répliquée.

>[!Define] Mongos
>Routeur de MongoDB qui redirige les différentes requêtes vers les différents shard.

Il est possible d'avoir plusieurs routeurs Mongos pour améliorer les performances et la disponibilité.

>[!Question] Sur quoi se base Mongos pour la répartition?

- Les métadonnées des différents shards (eg titre de a-c).

Stockés dans les config (server).

>[!Define] Config (server)
>Contient les métadonnées et les commandes de transfert des shards.

Les données ne sont pas directement stockés dans les shards mais sont stockés sous forme de chunks.

>[!Define] Chunk
>Bloc contenant une partie des données.

Taille défaut: 128 MB par chunk

Mongodb utilise la clé de sharding pour créer les chunks.

## Méthodes de Sharding

### Range Based Sharding
>[!Define] Range Based Sharding
>Répartit les chunks selon la valeur d'un champs (un chunk correspond aux données qui ont, pour cette clé, une valeur dans un certain intervalle).

>[!success] Avantages
>Facile de rediriger le travail pour Mongos.

>[!failure] Inconvénients
>Peut donner une distribution des données non-équitable.

### Hash Based Sharding
>[!Define] Hash Based Sharding
>Utilise une fonction de hashage à partir d'un champ pour placer la donnée.

>[!success] Avantages
>Permet une distribution aléatoire automatiquement aléatoire des données.

>[!failure] Inconvénients
>Mongos ne peut pas diriger aussi facilement les requêtes.