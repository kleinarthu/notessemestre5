# NotesSemestre5

## Author

KLEIN Arthur

## Beginning date

25/09/2023

## Description

Ce dépôt contient les notes de cours de mon semestre n°5 en ING3 ICC.

## Utilisation

N'hésitez pas à utiliser ce dépôt (lecture, copie) facilitant l'apprentissage d'autres élèves de Cy-Tech. Se laisser valoriser par mon travail valorise aussi le mien.  
Merci de me signaler tout constat pouvant indiquer une erreur ou un oubli de ma part (voir section Contact me)

## Organisation

Chaque matière est représentée par un dossier. Ces dossiers contiennent d'une part les notes de séances nommées Seance\<n\>.md, et d'autre part les notes de matières nommées d'après le nom de la matière. Les matières dont l'enseignement est divisé en plusieurs parties pourront refléter cette séparation dans les dossiers. Des fiches de préparation pour les partiels seront ajoutées en temps voulu.

## Format

Le format des documents de ce projet est le format markdown général auquel s'ajoutent les fonctionnalitées nativement lues par obsidian.

## Recommended software

[obsdidian](https://obsidian.md/) est le logiciel utilisé pour créer ces notes et est également le logiciel conseillé pour visualiser celles-ci.

## Contact me

email de cy-tech: kleinarthu@cy-tech.fr  
email alternatif: arthur2klein@proton.me  
telephone: +337 80 09 86 31