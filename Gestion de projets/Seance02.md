# eXtreme Programming
## Caractéristiques
- Compatible avec Scrum,
- Promeut la collaboration,
- Introduit la notion de Spike (difficulté),
- Axé sur la qualité du code,
- Tests obligatoires,
- CICD.

## Rôles
- Programmeur: concepteur et/ou codeur et/ou testeur,
- Client: product owner,
- Testeur,
- Coach: équivalent du scrum master,
- Tracker,
- Manager.

## Peer Programming
>[!Define] Peer Programming
>Deux personnes qui travaillent sur une même machine.

Les binômes changent de rôle régulièrement, et changent de membres.

## Spike

Difficulté dans une User Story => Création d'un Spike
>[!Define] Spike
>User Story adapté à la difficulté en intégrant de la R&D.

## Dette technique
>[!Define] Dette technique
>Mettre du code de mauvaise qualité en programmation.

=> Refactoring nécessaire plus tard pour éviter l'accumulation des tickets

## Code Smells
>[!Define] Code Smells
>Sert à identifier la dette.

- Conventions de nommages pas respectées,
- Trop de paramètres,
- Méthodes trop longues,
- Classes trop longues,
- Types primitifs,
- Couplage d'attributs,
- Trop de If/Switch (vs polymorphisme ou composition),
- Non-respect de DRY, YAGNI, KISS, ...

## Convention de code
>[!Example]
>- PHP: [PSR-2](https://www.php-fig.org/psr/psr-2/),
>- JS: [AirBnB](https://github.com/airbnb/javascript).

## Principes
>[!Define] DRY
>Don't Repeat Yourself: Ne pas répêter de code plus de 2 fois.

>[!Define] YAGNI
>You Ain't Gonna Need It: Faire des fonctionnalités qui n'ont pas été demandés.

>[!Define] KISS
>Keep It Simple, Stupid: Envisager d'abord les solutions simples.

>[!Define] SOLID
>- Single Responsibility,
>- Open to extension/Closed to change,
>- Liskov Substitution: les classes filles doivent pouvoir être utilisées à la place des classes mères,
>- Interface Segregation: Plus de petits interfaces,
>- Dependency Inversion: Dépendre des interfaces plutôt que des classes.

>[!Define] TDD
>Test Driven Development: Tests unitaires avant le code.

>[!Define] BDD
>Behavior Driven Development: Tests d'acceptation avant le code.

## 3 amigos
>[!Define] 3 amigos
>Session de travail qui mettent en place un PO, un développeur, et un testeur pour écrire des tests.

>[!Define] Gherkin/Cucumber
>Syntaxe des tests.

>[!Example]
>```Gherkin
>Feature: Basic Arithmetic
>
>Background: A Calculator
>	Given a calculator I just turned on
>
>Scenario: Addition
>	When I add 4 and 5
>	Then the result is 9
>```

>[!Define] Test unitaire
>Teste une seule fonctionnalité.

>[!Define] Test d'intégration
>On test la collaboration de l'objet (System Under Test) avec les méthodes du test double.
>On utilise souvent des stubs ou mocks.

>[!Define] Stub
>Code qui se comporte comme ... mais ne fait rien.

>[!Define] Mocks
>Imitation du comportement plus complexe que le stub.

>[!Define] Test de validation
>Condition le plus proche possible de l'utilisation réelle dans son ensemble.

>[!Define] Norme de qualité
>ISO 25010