# HUPI
## Présentation
- Entreprise créée en 2013 spécialisée dans le Big Data pour analyser les risques et les habitudes de consommation,
- Présente au pays basque, à Bidart et San Sebastian,
- Donne des recommendations automatiques aux clients,
- Aide les entreprises qui ont des problèmes spécifiques (avis négatifs par rapport aux clims des bus),
- Une grande partie du temps pour comprendre le métier du client,
- SCRUM et Agile.

## Cheminement de la solution
1. Récupérer des données brutes en utilisant un langage de programmation adapté à leur format, volume, et stockage,
2. Traitement analytique et statistique,
3. Intégrer ses données dans les systèmes métiers, soit à l'aide des outils d'HUPI, soit dans les systèmes du client.

## Méthodologie
4 sous-ensembles:
1. Comprendre l'aspect client et l'approche analytique,
2. Data requirements, Data collection et Data understanding,
3. Data Preparation et modeling,
4. Evaluation, Deployment et Feedback.

## Déroulement des projets
1. Spécification (1-2 mois),
2. Modélisation (2 mois),
3. Restitution (1-2 mois).

# Big Data
>[!Define] Big Data
>Bloc de données, mais aussi données non-structurées (photos, vidéos, audio, donnée autre).
## Applications
- Production: Prédiction de panne, maintenance, demande,
- Vente: Prédiction de recommendations, prix, ciblage,
- Santé: Alertes et diagnostics,
- Tourisme: Planification, prix, retours du client,
- Finance: Analyse des risques, campagnes marketing, rentabilité des investissements,
- Énergie: Analyse de l'utilisation, données sismiques, émission carbone.

## Collecte des données
- Collectées partout,
- Interaction avec les données multipliée par 50 entre 2010 et 2020,
- 1.7 Mb/s par personne en 2020,
- Non-structurée pour 80-90% des données.
![](https://cdn.statcdn.com/Infographic/images/normal/25443.jpeg)

## Différents types de données
### Données structurées
- Organisée,
- Bases de données relationnelles dans des Data warehouse,
- Difficile à mettre à jour.
### Données non-structurées
- Non organisée,
- Bases de données non-relationnelles dans des Data lakes,
- Facile à mettre à jour.

## Stockage distribué
Quand on veut stocker plus de données, on utilise plusieurs serveurs.
### Découpage horizontal
Répartir les lignes en fonction des serveurs que l'on a à disposition:
1. Chercher la variable qui a le moins de valeurs différentes,
2. Distribuer selon les valeurs de cette variable.

On peut aussi utiliser des modulo pour regrouper les classes.
