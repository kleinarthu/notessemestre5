# Stack and Owning
>[!Define] ASLR
>Laisse des petits aléatoires espaces entre les différents segments en RAM du programme pour éviter d'utiliser les addresses de retours en dépassant la taille de la stack.
## Traits
>[!Define] Trait
>Équivalent des interfaces.

>[!Example]
>- Copy: copie superficielle,
>- Clone: copie profonde.
## Owning and Borrowing
>[!Define] Owning
>Prendre possession de la variable (elle ne pourra plus être utilisé par le précédent propriétaire.

>[!Define] Borrowing
>Emprunter la variable.

>[!Important]
>On ne peut emprunter qu'une fois une variable de façon mutable.

## Généricité
Il est possible de faire:
```rust
struct Point<T> {
	x: T,
	y: T,
}
impl<T> Point<T> {
}
```

### Spécifier l'interface du template
```rust
struct Point<T: Copy, Clone> {
}
struct Point<T>
	where T: Copy, Clone {
}
```

## Option
```rust
enum Option<T> {
	Some(T),
	None,
}
```

# Refcell et Rc
>[!Example] Problème des listes doublement chaînées
>Si on fait une liste doublement chaînée, chaque élément à une référence vers l'élément de gauche ET l'élément de droite qui ont donc également une référence vers l'élément lui-même.
>=> **boucle de références**

>[!Define] Refcell
>Permet de ne tester les questions d'appartenances qu'au moment de l'exécution (et non de la compilation). Permet de faire ce que l'on veut en terme de mutabilité des pointeurs.

>[!Define] Rc
>Release Counter: compte les références à une variable encore utilisées (très proche d'un garbage collector).

## Imports
```rust
use std::cell::RefCell;
use std::rc::Rc;
```