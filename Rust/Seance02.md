# Stack vs Heap
L'utilisation de la heap nécessite un **appel système**.
L'utilisation de la stack n'est qu'une **opération algébrique**.
``` asm
mov esp [esp-16] ; esp = stack pointer -> on alloue de la place dans la stack et on "crée un trou"
```
-> Les allocations stack sont beaucoup plus rapide.

# Exemple de la factorielle
``` rust
fn factorielle(n: &u32) -> u32 {
    let mut res = 1u32;
    for i in 1..(*n + 1) {
        res = res * i;
    }
    return res;
}
```

# Tableau
``` rust
let mut v1 = Vec::new();
let mut v2 = vec![];
let mut v3 = vec![1, 2, 3];
```

# Closure
>[!Define] Closure
>Fonction qui a accès à des variables de contexte supérieur.

## Types de closure

Selon la mutabilité et le mode d'emprunt du contexte:
((((FnOnce / context) FnMut / &mut context) Fn / &context) fn / no context)

# Trait
``` rust
struct Square {
    side: f32,
}

struct Circle {
    radius: f32,
}

trait Shape {
    fn area(self: &Self) -> f32;
}

impl Shape for Square {
    fn area(self: &Self) -> f32 {
        self.side * self.side
    }
}

impl Shape for Circle {
    fn area(self: &Self) -> f32 {
        self.radius * self.radius * std::f32::consts::PI
    }
}
```