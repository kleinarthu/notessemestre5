# Introduction à Rust
>[!Info] Documentation
>[The Rust Programming Language](https://doc.rust-lang.org/book/)

## Gestion de la mémoire
Types de gestion de la mémoire:
- **Garbage collector**: on retire automatiquement les références non utilisées (du tas),
- **Gestion manuelle**: assignation manuelle dans certains segments: .text/.data/.bss/stack/partie réservée pour faire grandir le tas/heap/arguments/kernel.

Tous les programmes qui utilisent un garbage collector utilisent un **runtime**.

## Langage système
Rust est un **langage système**

>[!Define] Langage système
>Langage qui permet de faire du bas niveau (pas de runtime + instructions système).

>[!Success] Avantages
>Le garbage collector et les autres processeurs ne peuvent pas bloquer temporairement un appareil ayant de l'électronique embarquée.

## Compilation de code rust
- rustc crée un IR (indépendante du hardware et facile à optimiser),
- IR est compilé grâce à llvm.

## Paradigme RAII (Resource Acquisition Is Initialization)
En Rust, on déclare (presque) toujours une variable sur la stack:
### Borrowing
```Rust
let var(:u32) = 3(u32);
fn foo (p: u32) {
}
foo(var);
// var n'est plus utilisable car foo en a pris possession
```

```Rust
let var(:u32) = 3(u32);
fn foo (p: &u32) {
}
foo(var);
// var est encore utilisable car foo l'a simplement emprunté
```
Par défaut les variables sont immutable.
```Rust
let mut var = 3;
var = 4;
foo(var);
```

### Lifetime
```Rust
fn largest (&str1, &str2) -> &str {
	if str2.len > str1.len {
		str2
	} else
		str1
	}
}
```

Le résultat de la fonction a, par défaut, une durée de vie inférieure à celle de ses paramètres

``` Rust
fn largest (&'a str1, &'a str2) -> &'a str {
	if str2.len > str1.len {
		str2
	} else
		str1
	}
}
```

Les fonctions passent les objets par copie.

### Structures
Polymorphisme
``` Rust
struct MyStruct {
	pub a: u32
}

impl MyStruct {
	pub foo(&self)

	pub new() -> Self
}

let a = MyStruct::new();
let a = MyStruct {
	 a: 69
};
a.foo();
```
# Stack vs Heap
L'utilisation de la heap nécessite un **appel système**.
L'utilisation de la stack n'est qu'une **opération algébrique**.
-> Les allocations stack sont beaucoup plus rapide.

# Exemple de la factorielle
``` rust
fn factorielle(n: &u32) -> u32 {
    let mut res = 1u32;
    for i in 1..(*n + 1) {
        res = res * i;
    }
    return res;
}
```

# Tableau
``` rust
let mut v1 = Vec::new();
let mut v2 = vec![];
let mut v3 = vec![1, 2, 3];
```

# Closure
>[!Define] Closure
>Fonction qui a accès à des variables de contexte supérieur.

# Trait
``` rust
struct Square {
    side: f32,
}

struct Circle {
    radius: f32,
}

trait Shape {
    fn area(self: &Self) -> f32;
}

impl Shape for Square {
    fn area(self: &Self) -> f32 {
        self.side * self.side
    }
}

impl Shape for Circle {
    fn area(self: &Self) -> f32 {
        self.radius * self.radius * std::f32::consts::PI
    }
}
```
# Stack and Owning
>[!Define] ASLR
>Laisse des petits aléatoires espaces entre les différents segments en RAM du programme pour éviter d'utiliser les addresses de retours en dépassant la taille de la stack.
## Traits
>[!Define] Trait
>Équivalent des interfaces.

>[!Example]
>- Copy: copie superficielle,
>- Clone: copie profonde.
## Owning and Borrowing
>[!Define] Owning
>Prendre possession de la variable (elle ne pourra plus être utilisé par le précédent propriétaire.

>[!Define] Borrowing
>Emprunter la variable.

>[!Important]
>On ne peut emprunter qu'une fois une variable de façon mutable.

## Généricité
Il est possible de faire:
```rust
struct Point<T> {
	x: T,
	y: T,
}
impl<T> Point<T> {
}
```

### Spécifier l'interface du template
```rust
struct Point<T: Copy, Clone> {
}
struct Point<T>
	where T: Copy, Clone {
}
```

## Option
```rust
enum Option<T> {
	Some(T),
	None,
}
```

# Refcell et Rc
>[!Example] Problème des listes doublement chaînées
>Si on fait une liste doublement chaînée, chaque élément à une référence vers l'élément de gauche ET l'élément de droite qui ont donc également une référence vers l'élément lui-même.
>=> **boucle de références**

>[!Define] Refcell
>Permet de ne tester les questions d'appartenances qu'au moment de l'exécution (et non de la compilation). Permet de faire ce que l'on veut en terme de mutabilité des pointeurs.

>[!Define] Rc
>Release Counter: compte les références à une variable encore utilisées (très proche d'un garbage collector).

## Imports
```rust
use std::cell::RefCell;
use std::rc::Rc;
```

# Threads
>[!Define] Process
>- Géré au niveau kernel,
>- Lourd,
>- Créé par fork() sous Linux et CreateProcessA sous Windows.

Pas de runtime en rust par défaut.
Donc les threads sont des threads noyaux.
## Instanciation
```rust
use std::thread;
fn main() {
	thread::spawn(move || {
		// work
	});
}
```
## Communication avec les threads
### Mémoire partagée
```rust
use std::sync::mpsc;
fn main() {
	let (tx, rx) = mpsc::channel();
}
```
## Équivalent asynchrone de Rc\<Refcell
```rust
Arc<Mutex<>>
```

# Slice
>[!Define] Slice
>Morceau de collection.

>[!Example]
>- &str (pour string),
>- &\[T\] (pour Vec),

# Pointeurs intelligents
>[!Define] Box
>Pointeur vers une valeur dans le tas.

>[!Define] RC
>Pointeur permettant de partager plusieurs fois une même variable.

>[!Define] Refcell
>Permet de faire ce que l'on veut sur un pointeur.

>[!Define] Arc
>RC pour du code asynchrone.

>[!Define] Mutex
>Refcell pour du code asynchrone.

>[!Define] Cow
>Clone on write: Prête les variables de manière immutable et les clone quand elles ont besoin de l'ownership.