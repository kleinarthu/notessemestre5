# Introduction to Docker
## Problems with the deployment of software
>[!Failure] Deployment of software
>- Heavy softwares,
>- Hard to make a new version.

>[!Success] With contianer
>-  Decoupled services,
>- Fast improvements.

## Container
>[!Define] Container
>Isolates a running application into an execution context.

>[!Success] Advantages
>- Consistent en persistent infrastructure,
>- Less overhead compared to VM,
>- Isolated and automated.

## Difference between VM and Container
>[!Define] VM
>- Infrastructure,
>- Host Operating System,
>- Hypervisor,
>Per VM:
>- Guest OS,
>- Bins/Libs,
>- App.

>[!Define] Docker
>- Infrastructure,
>- Operating System,
>- Docker Engine,
>Per container:
>- Bins/Libs,
>- App.

>[!Failure] Inconvenient
>Can not have different OS between containers.

## Docker
### Before Docker
- No formalized format between OS,
- Harder to use,
- Have to install dependencies,
- Depends on the machine,
- Ship packages via deb, rpm, gem, jar, homebrew eg...

### Configuration
```shell
docker version
sudo groupadd docker
sudo gpasswd -a $USER docker
sudo service docker restart
```

### Change Docker network
```shell
sudo touch /etc/docker/daemon.json
```
Add
```
{
	"bip": "10.42.0.1/24",
	"dns": ["192.48.70.2","194.57.186.254","193.55.155.254","0.0.0.0"]
}
```
```shell
sudo service docker restart
```

### Docker architecture
- **Docker Engine**,
- **Docker Client**,
- **Docker Hub Registry**.

### Lancer docker
``` shell
docker run -it ubuntu /bin/bash
# Ubuntu sera téléchargé depuis les librairies de Docker
```

### Stop the container
``` shell
exit
```

### List the containers
```shell
docker ps
# To also check stopped
docker ps -a
docker start <container id>
docker attach <container id>
```

Containers can be non-interactive and run forever in the background.
### Non Interactive container
``` shell
docker run -d jpetazzo/clock
```

### Kill a container
```shell
docker kill <container id>
```

### Stop a container
```shell
docker stop <container id>
# 10 sec before TERM
```

### Detach from an interactive container

```shell
docker run -ti --detach-key ctrl-x,j <container>
# Specify the touch to stop the container
```

### Debugging
``` shell
docker exec <container>
```

## Docker images
>[!Define] Image
>- Collection of files + metadata,
>- Root filesystem of a container,
>- Read-only files,
>- Stack of layers adding, changing or removing files,
>- Able to share some layers between different images.

### How to change an image
1. Create a container with the image,
2. Do the modifications,
3. Create a new image from this container with:
```shell
docker commit
docker build
```

### Check the images available
```shell
docker images
```

### Download an image
```shell
docker pull wordpress:cli-1.2
```

### Remove a container
```shell
docker rm <container id>
```

### Remove an image
```shell
docker rmi <image id>
# only works if no container use this image.
```

### Build image
```shell
# Check difference
docker diff containerID

docker commit containerID image_name
```
