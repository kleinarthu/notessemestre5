# Introduction à Rust
>[!Info] Documentation
>[The Rust Programming Language](https://doc.rust-lang.org/book/)


## Gestion de la mémoire
Types de gestion de la mémoire:
- **Garbage collector**: on retire automatiquement les références non utilisées (du tas),
- **Gestion manuelle**: assignation manuelle dans certains segments: .text/.data/.bss/stack/partie réservée pour faire grandir le tas/heap/arguments/kernel.

Tous les programmes qui utilisent un garbage collector utilisent un **runtime**.

## Langage système
Rust est un **langage système**

>[!Define] Langage système
>Langage qui permet de faire du bas niveau (pas de runtime + instructions système).
>>[!Example]
>>- C,
>>- C++,
>>- Rust.

>[!Success] Avantages
>Le garbage collector et les autres processeurs ne peuvent pas bloquer temporairement un appareil ayant de l'électronique embarquée.

## Compilation de code rust
- Rustc crée un IR (indépendante du hardware et facile à optimiser),
- IR est compilé grâce à llvm.

## Concurrence avec C++
Tous deux bas niveau, juste au dessus de C en terme d'abstraction.
Linux est codé en C mais il y a des équivalence entre les types de C et ceux de Rust (ce qui ne serait pas le cas de Java).
>[!Success] Avantages
>Meilleure gestion de la mémoire (pas de fuite de mémoire),
>Plus simple syntaxiquement.
> Rust n'est pas un langage orienté objet (un peu plus fonctionnelle), mais possède quand même du polymorphisme.

>[!Failure] Inconvénients
>Incompatible avec C++ (langage le plus utilisé).

## Paradigme RAII (Resource Acquisition Is Initialization)
En Rust, on déclare (presque) toujours une variable sur la stack:
```Rust
let var(:u32) = 3(u32);
fn foo (p: u32) {
}
foo(var);
// var n'est plus utilisable car foo en a pris possession
```

```Rust
let var(:u32) = 3(u32);
fn foo (p: &u32) {
}
foo(var);
// var est encore utilisable car foo l'a simplement emprunté
```
Par défaut les variables sont immutable.
```Rust
let mut var = 3;
var = 4;
foo(var);
```

### Lifetime
```Rust
fn largest (&str1, &str2) -> &str {
	if str2.len > str1.len {
		str2
	} else
		str1
	}
}
```

Le résultat de la fonction a, par défaut, une durée de vie inférieure à celle de ses paramètres

``` Rust
fn largest (&'a str1, &'a str2) -> &'a str {
	if str2.len > str1.len {
		str2
	} else
		str1
	}
}
```

Les fonctions passent les objets par copie.

### Structures
Polymorphisme
``` Rust
struct MyStruct {
	pub a: u32
}

impl MyStruct {
	pub foo(&self)

	pub new() -> Self
}

let a = MyStruct::new();
let a = MyStruct {
	 a: 69
};
a.foo();
```