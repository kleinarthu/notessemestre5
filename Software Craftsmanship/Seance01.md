# But du Software Craftsmanship
Le mot bug est rentré dans le dictionnaire dans à peut prêt toutes les langues.
-> Comment faire mieux?

>[!Question] Comment est-ce que vous testez les choses?

# Pourquoi ne pas faire de tests?
- Temps,
- Pas essentiel (pour les cours),
- Peux d'impact,
- Test manuel,
- Difficile avec certains languages,
- Ennuyeux (pas l'impression d'avancer).

# Pourquoi faire des tests?
Environ 20-30% de temps de développement à tester ce que l'on vient de faire.
-> Réduire ce nombre (test automatisé)
Il n'y a qu'à écrire ce que l'on a dans la tête pour automatiser.

# Comment faire des tests?
> [!Question] Comment maximiser l'effectivité du temps de test?
> Tests automatisés (déterminer le nombre de tests à effectuer)

Plus un test est lent, plus on en veux.
-> On veux faire moins de tests end to end, que de tests d'intégration, que de tests unitaires.
![](https://3.bp.blogspot.com/-BoeAP-SpEfQ/Xdl5FNVGAzI/AAAAAAAABms/4lTHA_8Z0pQ8dC2trZhLnBf_G3DAl1-wwCLcBGAsYHQ/s1600/les-niveaux-de-test-pyramide-mike-cohn.png)

# Feedback
>[!Define] Boucle de feedback
>Temps nécessaire pour savoir si ce que l'on ajoute au code marche.

C'est le but du déploiement continu.
Facebook peut faire plusieurs mises à jour par minutes.
Le point de départ c'est l'automatisation des tests.

On veut savoir si on a casser quelque chose dès que l'on fait les plus petits changements, avant de finir la fonctionnalité.
-> TDD

# Qualités d'un test automatisé
Acronyme FIRST
>[!Define] FIRST
>- **Fast**: réduire la boucle de feedback, ne pas ralentir le développement,
>- **Independent**: changer le code d'un test ne doit pas changer le résultat de l'autre, les résultats de deux tests ne sont pas liés,
>- **Reliable**: le test doit être reproductible,
>- **Self-Checked**: Dit directement si ça passe ou ça ne passe pas (automatisé),
>- **Timely**: Écrit au moment où on écrit le code qui va avec (en premier).

Compromis à faire quand on se déplace dans la pyramide des tests.
Le premier qui "saute", c'est la reproductibilité ou l'indépendance.
>[!Example] Lancer de dés
>On veux faire un test pour une application qui lance un dés et retourne le résultat.
>On est obligé de faire une batterie de test parce que le code n'est pas contrôlable (aléatoire).
>-> Jouer sur les stats et non le résultat qu'on a à la fin.

# TDD
>[!Question] Qu'est ce qu'il se passe si la toute première ligne de code écrite cause le bug?

Si le test est ajouté plus tard, on est pas sûr si c'est le test qui est faux ou si c'est le code.

-> Faire les tests en premier.

La compilation est un test.

## Déroulement
1. On écrit un test rouge,
2. On va le faire passer au vert avec le plus petit effort possible,
3. On fait du refactoring (simplifier ce qui se répéte trois fois),
4. Reprendre au 1. et écrire le test suivant.
Si le code ne passe pas au vert pour l'étape 2, soit la modification était fausse, soit le pas que l'on a essayé de faire était trop grand.
On arrête le cycle quand on arrive plus à passer au rouge.

>[!Define] Refactoring
>Changer la structure d'un code sans en changer le comportement (les tests restent vert).

-> La mesure de la fonctionnalité à un instant T est donné par les tests qui sont écrit.

- 15% du temps à écrire un test rouge,
- presque 0% à le faire passer au vert,
- 85% du temps à faire du refactoring (préparer le terrain pour le test suivant).

>[!Define] Écouter vos tests
>Si vous avez du mal à faire passer vos tests, le problème n'est pas le test, c'est le code.

>[!Important]
>Parfois, on a envie de faire des tests manuel. C'est important, mais ça ne doit pas être les tests concernant le fonctionnement du logiciel, mais plutôt son utilisation, son ergonomie.

Les tests manuels doivent être vus comme des tests exploratoires.

# Examples de bonnes ESN
Beaucoup sont sur Paris, mais il y en a sur Bordeaux, Rennes, ...

- Zemika (Bordeaux, Lille, Nantes, Paris, ...),
- Shodo (Nantes et Paris),
- Codeworks (peut-être?),
- Octo,
- Arolla.

Éviter les "grosses" ESNs.

Ne pas avoir peur de changer de boîte  (tous les 3 ans eg) (14 postes ouverts par développeur).

# Librairies de test
- Java: Unittest, AssertJ,
- Python: pytest,
- C: c_unit,
- Typescript: Jest, Chai,
- Rust: Cargo, spectral,
- C++: google-test.

>[!Define] Test unitaire
>Full in memory.

>[!Define] Test d'intégration
>On commence à dépendre du système.

>[!Define] Mutation testing
>Tester les tests