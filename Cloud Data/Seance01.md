# Introduction au Web sémantique
## Historique
- 1945: Vannevar Bush écrit "As we may think": le nombre d'articles scientifiques croit trop vite: propose **Memex**,
- 1965: Ted Nelson propose une application du Memex: découper les fichiers en morceaux, enregistrer des morceaux, et créer des liens entre eux -> **hypertexte/hypermedia**,
- 1990: Tim Berners Lee propose que l'hypertexte soit distribué sur plusieurs machines -> **web**.

## Caractéristiques du Web
>[!Define] Web
>Ensemble de pages reliées entre elles par des hypertextes qui mènenet vers des adresses web.
### Composants de l'architecture client/serveur
- Identification et adressage (**URL**),
- Communication / protocole (**HTTP**),
- Langage de représentation (**HTML**).
### Standardisation
Permet l'inter-opérabilité (peuvent communiquer ensemble).
### La guerre des navigateurs
>[!Question]
>Qui va prendre le contrôle du web? Qui propose les formats de pages?

=> W3C

>[!Define] W3C
>**World Wide Web Consortium**: Harmonise l'évolution du Web et le standardise de façon à garantir l'interopérabilité.
### Moyens d'accès:
- Adresse Web,
- QR code.
=> Ne se limite plus au monde informatique.

>[!Define] URL
>Uniform Resource Locator: Identifie ce qui existe dans le web.

>[!Define] URI
>Uniform Resource Identifier: Identifie, sur le web, tout ce qui existe (même si ce n'est pas une page web).

>[!Define] IRI
>Internationalized Resource Identifier: Identify sur le web, dans n'importe quelle langue, ce qui existe.

## Web sémantique (WS)
### Web 2.0
- Pages purement syntaxique,
- Conçu pour être utilisé par des humains,
- Limite la recherche, l'extraction, la maintenance et la génération d'information.

=> Besoin d'ajouter une couche de connaissance au dessus du Web pour être utilisée par les ordinateurs uniquement. => **web sémantique**

### Définition
>[!Define] Web sémantique
>Prolongement du web qui ajoute une signification clairement définie aux informations pour permettre une plus étroite collaboration entre l'humain et la machine.

- Ensemble de connaissances (plutôt que de connaissances),
- Basé sur XML et RDF (plutôt que HTML),
- Basé sur les concepts (et non les mots-clés).
### Standard du WS
![](https://www.w3.org/DesignIssues/diagrams/sweb-stack/2006a.png)

## RDF: Resource Description Framework
>[!Define] RDF
>Standard du Web sémantique pour la description des ressources.

Repose sur les URI,
Ensemble de déclaration
>[!Define] Déclaration
>Sujet, Prédicat (propriété), Objet

>[!Example]
>index.html -> Creator -> Berners lee


=> **graph**

On représente les littéraux par des carrés dans le graphe.

### Name Spaces
Réduit le nom des ressources et prédicats utilisés en remplaçant un uri par un préfixe (eg local:name à la place de http://mon_site.fr/chemin/name)
#### Préfixes normalisés
- rdf,
- rdfs
- dc,
- owl,
- xsd,
### Format de sérialisation
![](https://www.w3.org/2013/dwbp/wiki/images/thumb/1/17/RDFSerialization-formats.png/800px-RDFSerialization-formats.png)
#### RDF/XML
```xml
<rdf:Description rdf:about="Sujet">
	<dc:predicat rdf:resource="Objet">
</rdf:Description>
```

#### Turtle
```ttl
@base <http://www.cat.com/docs> .
@prefix cs: <http://www.cat.com/schema/> .
@prefix dc: <http://purl.org/dc/elements/1.1/> .
R20301 a cs:PersonalDoc ;
	dc:creator auth:R051156 ;
	dc:title "Karin Hoepage" ;
	dc:date "2021-01-20"^^xsd:date .
```
## RDFS
>[!Define] RDFS
>RDF Schema s'écrit à l'aide de triplet RDF et spécifie les clases des ressources que vérifieront leurs instances

>[!Info] Documentation
>https://www.w3.org/TR/rdf12-schema/