# Terraform
## Qu'est ce que c'est?
>[!Define] Terraform
>Outil d'orchestration couplé avec un fournisseur cloud pour provisionner des serveurs, des bases de données, des load-balancers, ...

Créé par HashiCorp.
Version Opensource: OpenTF

L'orchestrateur crée un fichier d'état TFState.

## Configuration
Contrôlé par un fichier .tf écrit en HCL (HashiCorp Configuration Language).

`terraform init`

3 outils:
- `terraform plan`: visualise ce que l'on va faire,
- `terraform apply`: Crée la ressource par un appel API au provider pour régler la différence entre l'état chez le provider et le .tf et crée un fichier .tfstate qui permet de contrôler la machine (à ne pas supprimer),
- `terraform destroy`.

## Providers
`terraform providers` pour voir tous les provider.
```tf
provider "aws" {
	region="eu-west2
}
```

## Topologie
```topology.tf
resource "aws_instance" "example" {
	ami = "ami-760aaa0f"
	instance_type = "tf.micro"
}

output "id" {
	value = aws_instance.example.id
}
```

>[!Important] Fixer les versions des providers pour contrôler le fonctionnement.

# HCL
> [!Define] HCL
> Langage de configuration de Terraform.

Compatible avec JSON.

## Resource
### Création
```tf
resource "type_de_resource" "nom_de_resource" {
	attribut = "valeur"
}
```

### Récupération
`<type>.<nom>.<attribut>`
`"${<type>.<nom>.<attribut>}"`

### Précédence
Plus prioritaire:
1. CLI,
2. \*.auth.tfccars,
3. terraform.tfvars,
4. VAR ENV,
5. Varleur par défaut.

### Variable statiques
```tf
variable "tp" {
	type = string
	default = "tp42"
}

locals {
	bdd_name = "${var.tp}-bdd"
	web_name = "${var.tp}-web"
}
```

## Datasource
>[!Define] Datasource
>Récupère les informations non-gérées par terraform (eg régions) en plaçant le filtre entre curly brackets.

```tf
data "aws_region" "current" {
}

data "aws_vpc" "my_vpc" {
	cidr_block = "192.168.1.0/24"
}
```

## Output
>[!Define] Output
>Permet de récupérer certaines valeurs avec `terraform output`.

## Métaparamètres
### lifecycle
- create_before_destroy,
- prevent_destroy,
- ignore_changes.
### depends_on
Fournit une liste d'éléments à créer avant celui-ci.
### count
Pour faire des boucles sur count.index
### for_each
Prend en valeur = toset(\[...\])
foreach.key pour récupérer l'élément actuel.

## Séparation des fichiers
- variables.tf,
- datasource.tf,
- terraform.tfvars,
- bacckend.tf,
- provider.tf.