# Enjeux et perspectives
## Objectifs
- Faire de la **veille** informatique.
- (Commencer à creuser le sujet de PFE.)
- Apprendre collectivement à **s'informer** sur un nouveau sujet: bibliographer, le prendre en main, le restituer, ...
- Faire une **présentation** de manière pédagogue (tuto).

## Sujet
N'importe lequel.
>[!Exemple]
>- Bases de données,
>- Framework,
>- Unity,
>- Logiciels pour unifier les requêtes entre les systèmes de bases de données différentes,
>- Vercel,
>- Kotlin,
>- ...

Mais:
- pas de techno payantes,
- en lien avec l'option,
- avoir l'esprit ouvert,
- récent,
- devra être validé.

>[!Failure] Mauvais sujets
>Comparaison SQL/noSQL (parce que ce ne sont pas des technos comparables).

## Séance d'aujourd'hui
1. Trouver un groupe (de 3 majoritairement et compléter par 2 groupes de 4 ou un groupe de 2),
2. Trouver un sujet,
3. Les enregistrer dans le fichier Excel sur Teams.

## Disponibilité
1. Dans le bureau le matin,
2. En réunion en 216 l'après-midi.

## Trouver le sujet
Voir lien en fin de [[Réunion de rentrée]].

## Nombre de séance
- 21 heures,
- 2 séances par période.

## Forme
- Article de blog,
- Notebook ([jupyter](https://jupyter.org/), [zeppelin](https://zeppelin.apache.org/)),
- Packager avec Docker.

>[!Warning] Attention
>Ne pas avoir trop d'ambitions (garder le sujet pour le PFE).
>L'idée n'est pas de faire une étude en générale, mais plutôt de faire découvrir une techno.
