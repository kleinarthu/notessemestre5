# Applications
- Véhicules autonomes (reconnaître voitures),
- Médecine (détecter les tumeurs),
- Stocke Market,
- ...

# Architecture d'un Multilayer Percoptron (MLP)
![](https://miro.medium.com/v2/resize:fit:563/1*4_BDTvgB6WoYVXyxO8lDGA.png)

## Opération de changement de couche
$y = f((x_1 \times w_1) + (x_Z \times w_2) + b)$
- $y$: sortie,
- $w_i$: poids,
- $x_i$: entrée,
- $f$: fonction d'activation,
- $b$: biais.
$f$, le nombre de couches et le nombre de neurones par couche sont des hyperparamètres. Il est fixé pour le réseau de neurones.
$w_i, b$ sont des paramètres et ils varieront avec l'entraînement du modèle.

## Fonction d'activation
Ajoute de la non-linéarité dans le réseau de neurones.

>[!Example]
>- Linear Activation Function,
>- Logistic Activation Function,
>- Softmax,
>- Sigmoid.

## Évaluation: Fonction de perte
>[!Important] Mean Square Error
>$MSE = {1 \over n} \times \sum _ {i = 1} ^ {n} (y_{true} - y_{pred}) ^ 2$

>[!Important] Mean Absolute Error
>$MAE = {1 \over n} \times \sum _ {i = 1} ^ {n} |y_{true} - y_{pred}|$

>[!Important] Binary Cross-Entropy/Log Loss
>$CE Loss = {1 \over n} \times \sum _ {i = 1} ^ {n} -(y_i \times log(p_i) + (1 - y_i) \times log(1 - p_i))$

>[!Important] Categorical Cross-Entropy Loss
>$CE Loss = {1 \over n} \times \sum _ {i = 1} ^ {n} \sum_{j = 1}^M y_{ij} \times log(p_{ij})$

## Entraînement: descente de gradient
On peut écrire la fonction de perte comme une fonction de nos paramètres.
$L(w_1, w_2, w_3, w_4, w_5, w_6, b_1, b_2, b_3)$
On en calcule la dérivée pour chercher un minimum local.
${{\partial L} \over {\partial w_1}} = {{{\partial L} \over {\partial y_{pred}}} \times {{\partial L} \over {\partial w_1}}}$
or:
${{\partial L} \over {\partial y_{pred}}} = {{\partial(1 - y_{pred})^2} \over {\partial y_{pred}}} = {-2 \times (1 - y_{pred})}$
${{\partial y_{pred}} \over {\partial w_1}} = {{{\partial y_{pred}} \over {\partial h_1}} \times {{\partial h_1} \over {\partial w_1}}} = {w_1 \times f'(w_5 h_1 + w_6 h_2 + b_3)}$

Soit on met à jour à chaque itérations, soit on calcule un erreur moyenne.

# Introduction to CNN

## But
Reconnaissance d'image (reconnaissance faciale, voitures autonomes...).

## Architecture
- Input,
- Ensemble de filtres (convolution),
- Fully connected Neural Network.

>[!Question] Pourquoi utiliser les convolutions?
>Les filtres convolutionnels permettent d'extraire certaines information particulière de l'image (par exemple les arrêtes).
>Ça ne coûte pas grand chose en terme de paramètres mais ça permet de simplifier le réseau de neurones.

>[!Define] Padding
>Ajouter des cases vides en dehors de la grille pour garder le même dimension en sortie.

# Generative Adversarial Network
## Discriminant
>[!Define] Discriminator
>Distingue si l'image est une vraie image ou une fausse image.
### Entrée
- Vraies données,
- Fausses données générées par le générateur.

### Fonctionnement
- Classifier les images,
- Calculer la fonction de LOSS,
- Mettre à jour les poids.

## Générateur
>[!Define] Generator
>Modèle qui permet de produire des données vraisemblables (image, ...) à partir d'un vecteur de données aléatoire.

### Fonctionnement
- Produit un bruit aléatoire,
- Génère une sortie,
- Fait classifier l'image par le discriminant,
- Calcul la fonction Loss pour que le discriminant ne distingue pas l'image,
- Mettre à jour les poids du discriminant,
- Mettre à jour les poids du générateur.

=> C'est celui qui se trompe qui se corrige.

## Problèmes
>[!Define] Vanishing Gradient
>Si le discriminant est trop bon, le générateur ne reçoit plus assez d'information pour s'améliorer.

>[!Define] Mode Collapse
>Si le générateur est trop bon, le générateur va se reposer sur ses acquis et continuellement générer les mêmes images.