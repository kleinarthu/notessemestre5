# SPARQL
>[!Define] SPARQL
>**Simple Protocol And RDF Query Language**: Language et protocole de requête pour l'interrogation de méta-données RDF.
## Utilité
- Extraire l'informations sous forme de URI,
- Extraire des sous-graphes,
- Construire des nouveaux graphes.

## Syntaxe
### Sélection
```sparql
PREFIX NameSpace
SELECT ?Variable
WHERE {
	modèle de requête (motif)
}
```

>[!Example]
> Tout extraire :
>```sparql
>SELECT ?subject ?predicate ?object 
>WHERE {
>	?subject ?predicate ?object .
>}
>```

### Limiter le nombre de sorties
```sparql
SELECT *
WHERE {
	?s ?p ?o.
}
LIMIT 20
```

### Filtrer
```sparql
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
SELECT *
WHERE {
	$s rdfs:label $o.
}
```

a remplace rdf:type

### Comptage
```sparql
SELECT ?name count(?film) AS ?nbreFilms
WHERE {
	?producer dbp:name ?name
	?film a dbo:Film;
		dboproducer ?producer.
}
GROUP BY ?name
ORDER BY DESC(?nbreFilms)
```

### Filter
```sparql
SELECT ?name count(?film) AS ?nbreFilms
WHERE {
	?producer dbp:name ?name
	?film a dbo:Film;
		dboproducer ?producer.
FILTER (REGEX(?name, "Thomas.*"))
}
GROUP BY ?name
ORDER BY DESC(?nbreFilms)
```

### Contrainte optionnelle
```sparql
SELECT ?name ?quote
WHERE {
	?film a dbo:Film;
		dbp:name ?name;
		dboproducer dbr:James_Cameyron.
	OPTIONAL(?film dbr:quote ?quote)
FILTER (REGEX(?name, "Thomas.*"))
}
GROUP BY ?name
ORDER BY DESC(?nbreFilms)
```

### Combiner des tables
Suivre le bloc WHERE par un bloc UNION.

# Linked Open Data (LOD)
Application du web sémantique.
>[!Define] LOD
>Ensemble de jeux de données publiées sur le Web qui utilise les technologies du Web Sémantique pour lier les données de chaque autres graphes.

## Principes de base
- Identifier les choses avec des URI,
- Les URI doivent être compatibles avec le web du document (commencer par http),
- Fournir des renseignements lisibles par les humains et par les machines en utilisant RDF ou SPARQL,
- Les URI donnent accès à d'autres URI pour stimuler l'interconnexion.

## Principal acteur: DBPedia
- Créée en 2007,
- Proviens de Wikipedia,
- 768 classes,
- 3000 propriétés.

## Wikidata
Contient plus de 100 millions d'éléments

## Mode d'accés
>[!Define] SPARQL Endpoint
> Utilise SPARQL pour accéder aux connaissances de LOD

>[!Example]
>- https://dbpedia.org/sparql

# Linked Open Vocabularies (LOV)
>[!Question] Quel vocabulaire utiliser pour uniformiser les propriétés?

=> LOV

>[!Define] LOV
>Catalogue de vocabulaires disponible pour être réutilisés dans le but de décrire des données sur le Web.
>Combine les ontologies utilisés par plusieurs bases de connaissances.

>[!Example]
>FOAF (friend of a friend project)