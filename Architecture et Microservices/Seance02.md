# Python: Avancé
>[!Important]
>Pas de notebook pour ce cours.

## Programmation orientée objet en Python
### Déclaration de la classe
```python
class NomDeClasse:
	"""Description
	- attribut
	"""
	attribut = default_value # ou None
	_private_attribut = default_value # privé par convention

	def __init__(self, value): # constructeur
		self.attribut = value;

	def _methode_privee(self):
		return; 

	def methode_publique(self, value);
		self._methode_privee();
		return value
```
### Déclaration de l'objet
```python
objet = NomDeClasse(5)
```
### Voir les méthodes d'une classe
```python
dir(nomDeClasse)
```
### Voir les attributs d'un objet
```python
objet.__dict__
```
### Encapsulation
>[!Define] Propriété
>Ensemble de fonctions qui n'affectent qu'un attribut d'un objet. Assigner une property modifiera le comportement par défaut lorsque l'on récupère, modifie, ... l'attribut.

```python
residence = property(_get_residence, _set_residence, _delete_residence, _aide_residence)
```
### Méthodes spéciales
#### Suppression d'un objet
```python
def __del__(self):
	pass
```
#### Représentation d'un objet
```python
def __repr__(self):
	return self.__str__
```
#### Conversion d'un objet
```python
def __str__(self):
	return ""
```
#### Autres
```python
# Conteneurs
__getitem__
__setitem__
__delitem__
__contains__# (in)
__len__
# Numérique
__add__
__sub__
__mul__
__truediv__
__floordiv__
__mod__
__pox__
# Comparaison
__eq__
__ne__
__gt__
__ge__
__lt__
__le__
```
### Héritage
```python
class Parent:
	def __init__(self):
		pass

	def methode(self):
		pass
	
class Enfant(Parent):
	def __init__(self):
		super().__init__(nom);

	def methode(self): # surcharge
		pass
```
### Héritage multiple
```python
class Parent1:
	def __init__(self):
		pass

class Parent2:
	def __init__(self):
		pass

class Enfant(Parent1, Parent2):
	def __init__(self):
		Parent1.__init__(self)
		Parent2.__init__(self)
```
### Tester la nature d'un objet
#### Vérifier si on est une instance
```python
isinstance(alfa, Vehicule)
```
#### Vérifier si on est une sous-classe
```python
subclass(QuatreRoux, Vehicule)
```
## Déclaration d'erreurs
```python
class RandomException(Exception):
	def __init__(self message):
		self.message = message

	def __repr__(self):
		print(message)
```
## Lancer une exception
```python
try:
	if (input == 5):
		raise RandomException("message")
	else:
		print(input)
except RandomException as exception:
	print(exception.message)
finally:
	print("On finit le programme quand même."")
```
## Fonctionnel
### Décorateurs
```python
def decorator(fonction):
	def wrapper(*args, **kwargs):
		print("do something before");
		result = fonctino(*args, **kwargs)
		print("do something after");
		return result
	return wrapper

@decorator
def f(x):
	return x
```
### Lambdas
```python
f = lambda x, y: x + y
greeted_names = list(map(lambda name: f'Hi {name}', names))
div_by_5 = list(filter(lambda num: num % 5 == 0, numbers))
"""sum
max
min
zip
...
"""
```
## Pattern matching
```python
match point_cardinal:
	case Cardinal.North:
		print("On va au nord")
	case Cardinal.South:
		print("On va au sud")
	case Cardinal.East:
		print("On va à l'est")
	case Cardinal.West:
		print("On va à l'ouest")
	case _:
		print("Je ne sais pas")
```
## Tests unitaires: pytest
```python
import unittest

class NomDeClasse:
	def __intit__(self):
		pass

	def methode_a_tester(self):
		return 2

class TestNomDeClasse(unittest.TestCase):
	def test_methode(self):
		instance = NomDeClasse()
		self.asertEqual(
			instance.methode_a_tester,
			2
		)
```

```shell
pytest .
```

## Documentation
Deux standard:
- [sphinx](https://www.sphinx-doc.org/en/master/),
- **google docstring**.

```shell
sphinx-quickstart
sphinx-build -b html sourcedir builddir
```

# Utilisation
## BigData
- **PySpark**,
- **pandas** (manipulation de data frames).
## DevWeb
- **Flask**,
- **fast API**,
- **django**.
## Testing d'UI
- **Selenium**.
## Client lourd
- **tkinter**.
## Machine learning
- **scikit**.
## Deep learning
- **TensorFlow**,
- **PyTorch**.
# Pandas
>[!Define] DataFrame
>Matrice.

>[!Define] Series
>DataFrame à une seule colonne.
## Créer des séries
```python
s1 = pd.Series(np.random, randn(5), index["a", "b", "c", "d", "e"])
s2 = pd.Series(np.random, randn(5))
s2.index # return Index([0, 1, 2, 3, 4], drype = 'int')
```
## Créer des DataFrames
``` python
d = {
	 "one": pd.Series([1.0, 2.0, 3.0], index=["a", "b", "c"]),
	 "two": pd.Series([1.0, 2.0, 3.0, 4.0], index=["a", "b", "c", "d"])
} // Complété avec NaN
df = pd.DataFrame(d)
df = pd.DataFrame(d, index=["d", "b", "a"])
df = pd.DataFrame(d, index=["d", "b", "a"], columns=["two", "three"])
df.shape
df.empty
df.rename(columns=dict_from_old_to_new)
```
## Utiliser des csv
```python
pd.read_csv('data.csv')

df = pd.DataFrame({
	'name': ['Raphael', 'Donatello'],
	'mask': ['red', 'purple'],
	'weapon': ['sai', 'bo staff']
})
df.to_csv(
	'out.zip',
	index=False,
	compression = dict(
		method='zip',
		archive_name='out.csv
	)
)
```
## Sélection de données
```python
df['nom_colonne']['nom_ligne']
df[['a', 'b']] = df[['b', 'a']]
df['colonne'][::2]
s.loc['nom_ligne':]
s.iloc[index_nom_ligne: ]
df.loc[['nom_colonne_1', 'nom_colonne_2']:]
dfi.iloc[1:5, 2:4]
df[df['A'] > 0] # Sélectionne les lignes dont l'élément de colonne A à une valeur supérieure à 0
```
## Définition des index
```python
data.set_index('c')
data.set_index(['a', 'b'])
data.reset_index() # index numérique
```
## Combinaison
``` python
pd.concat([df1, df2, df3])
pd.concat([df1, df2, df3], axis = 1)
df1.append(df2)

pd.merge(df1, df2, on = "column")
# merge method:
# - left,
# - right,
# - outer,
# - inner,
pd.merge(df1, df2, how = "left", on = "column")
```
![](https://cartman34.fr/wp-content/uploads/2017/01/sql_joins.jpg)
## Données manquantes
```python
pd.isna(df["column"])
pd.notna(df["column"])
df.notna()
# naT est l'équivalent de nan pour les dates
df.fillna(0)
# Forward filling: copier la valeur précédente pour les nan
df.fillna(method="pad")
# Forward filling: copier la valeur suivante pour les nan
df.fillna(method="bfill")
df.dropna(axis=1) # supprime les colonnes contenant du nan
df.interpolate() # par exemple pour une timeseries (2 colonnes: temps valeur)
```
## Opérations sur les données
``` python
df.index.is_unique # boolean
df.drop_duplicates() # sur l'index
df.drop_duplicates(subset=['column'])
df.diff() # Descente de gradient -> retire la ligne n + 1 à la ligne n
df1.equals(df2)
```
## Copies de DataFrame
```python
deep = df.copy()
shallow = df.copy(deep = False)
```
## Parcours
```python
df.iterrows()
df.itertuples()
```