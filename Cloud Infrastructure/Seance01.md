# Introduction to Cloud Computing and Virtualization
## Assessment
- One single exam,
- Some exercises may be graded,
- An assignment about a given technology will be submitted and presented the last day.

## What is the cloud ?
Almost everything used in the internet is a cloud service.

### Little bit about history
Before Amazon created AWS, in the 90s, need for Utility Computing for IBM: a lot of computer were unused during the week-end + Evolution of the virtualization technology.
Proposed access to a single physical machine in exchange for a payment.
Later changed it to virtual machines.

### Traditional approach
single user / DMZ / LAN
Then every services was put in a virtual machine assembled in a cluster.

>[!Define] Cluster
>Ensemble of virtual services in a physical infrastructure.


### Definition by NIST
>[!Define] Cloud Computing
>**Cloud computing** is a model for enabling ubiquitous, convenient **on-demand network access to a shared pool of configurable computing resources** (e.g., networks, servers, storage, applications, and services) that can be rapidly provisioned and released with minimal management effort or service provider interaction. This cloud model is composed of five essential characteristics, three service models, and four deployment models.

## Virtualization
>[!Define] Virtualization
>Act of creating a virtual (rather than actual) version of something, including virtual computer hardware platforms, storage devices, and computer network resources.

Possible through an **hypervisors**.

### Types of virtualization
The guest OS no longer runs in ring 0.

#### Type 1: sort of operating system
>[!Define] Binary translation
>Translating every command of the guest OS on the file -> really slow.

>[!Define] Paravirtualization
>Replacement of "critical" of "dangerous" code to safe code in the source code.
>You recompile the kernel in the machine.

#### Type 2: new window
>[!Define] Hardware Virtualization
>Capture the privileged instructions, traps all exception and translate them to something that will be executed by the OS.

## Service models
Infrastructure Provider
n*(Hardware <-> OS <-> Virtualization Layer) <-> Infrastructure Management <-> n*(Service (for users)|Infrastructure Interface (for service provider)))

### IaaS
You provide an access to create virtual machines to have a small cluster (aws).

### PaaS
For example developing platform as a Service.

### SaaS
Software that runs on the cloud instead as on your computer (gmail).

## Deployment models
Private Cloud / Public Cloud / Hybrid Cloud

## Cloud providers and services
- AWS,
- Azure,
- Google Cloud,
 - Oracle.

Many services are similar between cloud providers(cloud storage, ...).

## Hidden cost
- Free Tier Antiquity,
- Under-utilized Elastic IP address,
- Unused elastic load balances,
- Unused Elastic block storage,
- Orphan elastic block storage snapshots,
- SI Access charges,
- Si partial uploads,
-  Transfer cost (out of the service).

## Introduction to OpenNebula
>[!Define]
>Uniform management layer in charge of orchestrating different technologies.

![](https://www.opensourceforu.com/wp-content/uploads/2017/01/Figure-1-OpenNebula-deployment-model.jpg)
