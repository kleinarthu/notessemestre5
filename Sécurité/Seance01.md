# Prise de notes pour l'examen
Noter les définitions des termes de vocabulaires pendant les séances pour l'examen.

# Professeur
Gilles Bouerie
## Certifications
Celles qui terminent par 1 sont certifiantes.
- ISO27001: Leader Implementer,
- ISO27005; Risk Manager,
- RSSI.
>[!Define] NISv2
>Directives européennes de cyber-sécurité (les employés et dirigeants doivent être formés à la cyber-sécurité).

>[!Info]
>[Norme ISO](https://www.iso.org/fr/standard/)

>[!Define] ANSSI
>Agence Nationale de Sécurité des Systèmes d'Information.

>[!Define] RSSI
> Responsable de la sécurité des systèmes d'information

>[!Define] ISO27001
>Norme internationale décrivant les bonnes pratiques à suivre dans le cadre de la création d'un système de gestion de la sécurité de l'information

>[!Define] ISO27005
>Fournit des aides et méthodes pour satisfaire ISO27005.

# Déroulé
1. Concepts (avec quelques vidéos, ne pas hésiter à interrompre),
2. Étude de cas,

# Piliers de la CyberSécurité
>[!Define] DIC
>- Disponibilité (veille à la continuité et à la reprise de l'activité),
>- Intégrité,
>- Confidentialité.

>[!Define] PRA
>Plan de Reprise d'Activité

>[!Define] PCA
>Plan de Continuité d'Activité

Pour la norme on rajoute un P (Preuve).

>[!Define] Non-répudiation
>On doit pouvoir prouver qu'un individu à fait ce qu'il a fait.

## Sécurité des accès
- Authentification,
- Autorisation,
- Gestion des identités,
- Contrôle des accès physiques (badges, clés) (flipperzero),
- Surveillance et audits,
- Surveillance réseau,
- Sensibilisation (ne pas éteindre un pc piraté pour pouvoir récupérer la clé dans la RAM),
 - Chiffrement,
 - Confidentialité,
 - Sécurité des protocoles,
 - Sécurité sans fil,
 - Normes de sécurité (RGPD, ISO27xxx, NISv2, PCI-DSS, DORA, PIPAA, ...).

>[!Define] CRC
>Centre de Recherche Cyber: Antennes de l'ANSSI.

>[!Define] Onboarding
>Entrée de l'employé dans l'entreprise

>[!Define] Offboarding
>Sortie d'un employé de l'entreprise

>[!Define] Shadow IT
>Activité IT au sein d'une entreprise qui échappe à la connaissance des services IT.

>[!Define] Clé DKIM
> DomainKeys Identified Mail: Clé d'authentification pour éviter l'usurpation d'adresse mail et le spam.

>[!Define] IDS
>Détection d'intrusion

>[!Define] IPS
>Prévention d'intrusion

>[!Define] MPSK/DPSK
>Multi-PSK/Differential-PSK: Chaque appareil a son propre code wifi.

# VPN
## VPN SSL/TLS
Les informations sont cryptées mais on connaît encore la source et la destination.
Avoir le VPN sur un client lourd et non le navigateur permet d'également protéger ce qui transite entre l'ordinateur et le navigateur.
>[!Example]
>- OpenVPN,
>- Cisco AnyConnect,
>- Pulse Secure,
>- FortiClient,
>- Barracuda SSL VPN,
>- ...

>[!Define] TCP
>Commence par un Triple Handshake avant la communication.

>[!Define] UDP
>L'information est directement communiquée.

## VPN IPSEC
![](https://upload.wikimedia.org/wikipedia/commons/thumb/8/8d/OSI_Model_v1.svg/langfr-330px-OSI_Model_v1.svg.png)
- Plus pour les connexion intra-site,
- Niveau 3 de la couche OSI,
- Peut être utilisé pour la communication entre un réseau privé et un utilisateur privé (rare car lourd à mettre en place),
- Souvent associé à d'autres protocoles (L2TP, ESP, IKEv1, IKEv2, ...),
- Possibilité de choisir de qui est sécurisé,
- L2TP: très peu utilisé, système conçu par Microsoft,
- Peut être très compliqué si matériel ou logiciels incompatibles.
>[!Define] Mode tunnel
>On chiffre la source et la destination

>[!Define] BGP
>Border Gateway Protocol: Protocole de routage dynamique pour gérer les changements de routes.

## MPLS
>[!Define] MPLS
>Multi-Protocol Label Switching

- À l'intérieur d'un réseau,
- Protocole qui transmet des données non chiffrées mais qu'elles soient écoutables,
- Tous les équipements doivent être équipés (coûte très cher),
- Garantit le débit et la qualité de service.

>[!Define] QOS
>Quality Of Service

## Réseau privé virtuel de connexion au Cloud
>[!Success] Avantages
>- débit,
>- latence.

>[!Failure] Défauts
>- Coût,
>- Pas toujours possible à mettre en place,
>- Contrainte sur les compatibilité.

>[!Define] SLA
>Service Level Agreement: Garantit de disponibilité du service.

>[!Define] uCPE
>[Universal Customer Premises Equipment](https://blog.beronet.com/fr/beronet-universal-customer-premises-equipment): Équipement déployé chez le client par l'opérateur de télécommunication.

>[!Define] XDR
>Extended Detection and Response: Met en corrélation plusieurs couches de données pour détecter les erreurs.

>[!Define] EDR
>Endpoint Detection and Response: Ne détecte que sur un Endpoint, basé sur l'IA

>[!Define] C2
> Command and Control: Fichiers qui permettent au hacker de revenir.

>[!Define] Sauvegarde 321
>- 3 versions,
>- 2 support,
>- 1 externalisé.

# Architecture SASE
>[!Define] Architecture SASE
>Plutôt que d'avoir les systémes à l'intérieur d'un firewall, on les laisse à l'extérieur mais tout n'est accessible que via un service de gestion de sécurité.

>[!Define] SWG
>Secure Web Gateway: Passerelle qui vérifie que les domaines du DNS sont corrects

>[!Define] CASB
>Cloud Access Security Broker

>[!Define] DLP
>DataLoss Protection: Outil qui empêche qu'on vous vole des données.

>[!Define] ZTNA
>Zero Trust Network Access.

>[!Important]
>MOOC à faire:
>- ANSSI (5-6h): définitif,
>- Sensibilisation à la CyberSécurité de Cisco.

# Authentification des utilisateurs
>[!Define] LDAP
>Annuaire d'utilisateurs et des droits des utilisateurs.

>[!Important ] Garder trace des commandes lancées
>```shell
>script -a -o traces.txt
>```