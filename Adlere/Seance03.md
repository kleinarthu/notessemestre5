# Ansible
>[!Define] Ansible
>Outil pour configurer la pipeline, créer des fichiers, déployer des applications, ...

Utilise une connection **ssh**.
2 manières de l'exécuter:
- À distance (ssh),
- En local si on a déjà installer Ansible sur la machine (moins utilisé).
## Playbook
>[!Define] Playbook
>Fichier yaml qui contient les instruction.

>[!Define] Plugin
>Morceau de code qui enrichit la fonctionnalité centrale d'Ansible.

>[!Define] Inventaire
>Liste des systèmes de votre infrastructure.

>[!Define] Roles
>Morceau de code réutilisable, obéissant à une logique (= module).

>[!Define] Colection
>Package apportant du contenu d'automatisation.

>[!Info] Certifiations
> Toutes les formations et certifications sont sur Red Hat (payantes).
## CLI
```sh
ansible-playbook -i hosts.ini ./install_software.yml
ansible-playbook --syntax-check <playbook.yaml>
ansible-doc -l # Ensemble de la doc disponible
ansible-doc debug # Informations sur le module debug
ansible -m ping
ansible-galaxy install <role pré-créé>
ansible-vault # Crypter les variables
```

### Options de ansible-playbook
- `-i inventory`
- `-u <user>`
- `-k` indique s'il faut un mot de passe,
- `-b` pour être root,
- ...

## Installation
Avec `pip install`.

## Inventaires
### Types
- Statiques,
- Dynamiques: généré par un autre fichier.
### Formats
- .ini,
- .json,
- .yaml.
### Example
```
ungrouped:
	hosts:
		mister-brown:
			ansible_host: 10.0.101.100
		blue.example.com:
		192.168.100.1:
```