# Les ontologies

>[!Define] Ontologie
>- Partie de la métaphysique (philosophie) qui étudie le **type** des choses,
>- Spécification explicite d'une **conceptualisation**,
>- **Compréhension partagée** d'un domaine d'intérêt,
>- **Spécification** formelle et explicite d'une conceptualisation partagée.

## Buts
- représenter au mieux les connaissances pour les machines et les humains,
- partager ces connaissances,
- ré-utiliser les connaissances.

Des ontologies sont créées pour plusieurs **domaines** (langage, médecine, connaissances, ...).

## Types d'ontologies
>[!Define] Top-Ontologie
>Structure les connaissances de haut niveau à l'aide de termes très généraux.
> Trop général pour être instancié.

>[!Define] Core-Ontologie
> Fournit les concepts structurant le domaine.

>[!Define] Ontologies de domaine
>Fournit les concepts du domaine utilisé.

But:
- Retrouver des éléments communs entre des domaines distincts,
- Avoir une interopérabilité entre les différentes ontologies.

## Composants
- **Concepts** désigné par un nom qui peut être accompagné d'une valeur instanciée par le concept ou ses individus,
- **Relations** entre différents concepts (un domain et un range),
- **Axiome**: Contraintes sur les connaissances, relation entre concepts et relations (propriété obligatoires, cardinalité, transitivité, ...).

## OWL
>[!Define] OWL
>**Web Ontology Language**: repose sur RDFS.
- classes,
- héritage (subClassOf),
- instances,
- propriétés (ObjectProperty si la valeur est un individu, DataProperty si la valeur est un littéral),
- caractéristiques de ces propriétés.

### Outils ajoutés par rapport à RDF
- Disjonction,
- Combinaison booléennes de classes,
- Restrictions de cardinalité,
- Caractéristiques de ces propriétés,
- Étendue locale des propriétés, du range.

Avec RDF, rien n'empêche de dire:
"pomme fuji" a pour frère "Julien"o

### Logiciel utilisé
[Protégé](https://protege.stanford.edu/)

>[!Define] Raisonneur
>Permet de conduire des raisonemnts et de dériver des conclusions à partir d'une base de connaissance pour vérifier sa cohérence et ajouter de nouveau faits.