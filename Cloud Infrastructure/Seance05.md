# Compte google
>[!Important] Compte google
>S'assurer de ne pas être sur son compte personnel.

>[!Important]
>Bien éteindre les VM non utilisées pour ne pas dépenser le crédit de son compte.

> [!Info] Deadline assignment
>4 jours avant l'examen.

# Kubernetes: Container orchestration
>[!Failure] Problem
>Manually managing several container with their networks can become difficult.

=> **Kubernetes** (= K8s)
>[!Define] Kubernetes
>Open source system to manage clusters of container.

## Architecture
>[!Define] Defined state
>Specified by the developer in a yaml or json file they submit to the API,

>[!Define] Actual state
>Compared to the Defined state by Kubernetes do update it.

>[!Define] Pods
>Group of one or more containers that share network and storage, managed as a single unit.

>[!Define] Node
>Worker machine (physical or virtual) in the cluster.

>[!Define] Master node
>Node that chose how to manage the network for the other nodes.

>[!Define] Deployment
>Update the actual state with the desired state.

The master automatically re-create the deleted or closed nodes.
## GCP (Google Cloud Platform)
Used for the ML of Google
## Pod
- Can run one or more container,
- Share networking and storage separate from the node,
- Defined with a YAML file.
>[!Example] pod.yaml
>```yaml
>apiVersion: v1
>kind: Pod
>metadata:
>	name: my-app
>spec:
>	containers:
>	- name: my-app
>	  image: my-app
>	- name: ngix-ssi
>	  image: ngix
>	  ports:
>	  - containerPort: 80
>	  - containerPort: 443
>```

## Deployment
- Ensure the number of pods running at the same time,
>[!Example] deployment.yaml
>```yaml
>kind: Deployment:
>apiVersion: v1.1
>matadata:
>	name: frontend
>spec:
>	replicas: 4
>	selector:
>		role: web
>	template:
>		metadata:
>			name: web
>			labels:
>				role: web
>		spec:
>			containers:
>			- name: my-app
>			  image: my-app
>			- name: ngix-ssi
>			  image: ngix
>			  ports:
>			  - containerPort: 80
>			  - containerPort: 443
>```

## Service
>[!Define] Service
>Abstract way to expose an application running on a set of pods as a network service.

>[!Example] service.yaml
>```yaml
>kind: Service
>apiVersion: v1
>metadata:
>	name: web-frontend
>spec:
>	ports:
>	- name: http
>	  port: 80
>	  targetPort: 80
>	  protocol: TCP
>	selector:
>		role: web
>	type: LoadBalancer
>```

## Labels
>[!Define] Labels
>Key-value pairs used in the pods.

>[!Example] labels.yaml
>```yaml
>"metadata": {
>	"labels": {
>		"key1": "value1",
>		"key2": "value2"
>	}
>}
>```