# Théorie du management situationnel
- Hersey Blanchard,
>[!Define] Management situationnel
>Il n'existe pas de style de management idéal, le manager adapte son style à la situation déterminé par le niveau d'autonomie des dollaborateurs.

>[!Define] Niveau d'autonomie
>Niveau de motivation + Niveau de compétences

## Lois
- L'efficacité vient de l'adaption du style à la situation,
- Il faut évaluer en permanence l'autonomie,
- Le manager doit créer les conditions propices au développement de cette autonomie.

## Styles
|               | Pas motivé         | Motivé           |
| ------------- | ------------------ | ---------------- |
| Pas compétent | Style directif     | Style explicatif |
| Compétent     | Style participatif | Style délégatif  |

# Brainstorm inverse
>[!Question] Comment vous feriez pour démotiver votre équipe?

## Relationnel
- Ne jamais leur parler,
- Être agressif,
- Organiser des combats quand certains ne sont pas d'accord,
- Parler dans leur dos,
- Diviser les employés,
- Toujours critiquer le travail,
- Ne jamais aller aux réunions et poser des questions sur ce qui s'est dit pendant les réunions,
- Appeler les gens pendant leur travail,
- Voler les idées des gens,

## Organisation
- Mettre beaucoup de meetings juste avant les horaires de départs,
- Raccourcir la pause déjeuner,
- Donner la même tâche à tout le monde et ne garder que la meilleure solution,
- Demander beaucoup de compte-rendus,

## Condition
- Être ferme sur les congés,
- Baisser les salaires,
- Ambiance froide,
- Faire un Wall of Shame,
- Environnement de travail pourri,
- Saboter le travail des employés,

- Obliger des teams building en dehors des heures de travail,
- Ne plus donner de travail,
- Travailler debout.