# Professeur
- Aline (tutoyer),
- alinecencetti@gmail.com.

# Programme
1. Sociologie des organisations,
2. Management (histoire, styles, ...).

# Évaluation
- Dossier personnel,
- 3-4 pages max,
- Décrire une situation de management vécue,
- L'analyser avec les outils vus en cours,
- À rendre vendredi sur Teams,
- Faire pendant les cours.

# Organisation
>[!Define] Organisation
>Une organisation est un **ensemble constitué** (on sait qui en fait partie) en vue d'atteindre un **but clairement défini** (impact sur le réel) et ayant à sa disposition un série de **moyens et de procédés de contrôle** permettant d'assurer la **subordination/adhésion de tous** à l'accomplissement de ce but.

## Caractéristiques
- Répartition des tâches et rôles,
- Division de l'autorité,
- Système de communication,
- Critères d'évaluation/contrôle.

## Pourquoi analyser?
- Comprendre,
- Prévenir les crises,
- Comprendre les jeux de pouvoirs,
- Comprendre les désaccord,
- Comprendre les effets systémiques (influences d'un événement au sein de l'organisation).

## Niveaux de l'organisation
- L'organisation (son cadre),
- Les individus,
- La société (la culture).

## Buts
### Buts de missions
Tous les buts qui ont trait aux produits, aux services et/ou aux clients de l'organisation.

### Buts de système
Concerne les buts où il est question de l'organisation et/ou de ses membres.

## Répartition du travail
### Horizontale
Parcellisation des tâches, simplifier les tâches.

### Verticale
Tâches de conception / exécution.

## Distribution du pouvoir
- centralisée,
- Décentralisée.

## Mécanismes de coordination
- Ajustement mutuel,
- Supervision directe,
- Standardisation des procédés,
- Standardisation des résultats,
- Standardisation des qualifications,
- Standardisation des normes.

## Étude de cas

TotalEnergies

| Buts                                                                | Division du travail                 | Distribution du pouvoir | Mécanismes de coordination                                          |
| ------------------------------------------------------------------- | ----------------------------------- | ----------------------- | ------------------------------------------------------------------- |
| Fournir et produire de l'énergie, rester premier fournisseur privé. | Verticale forte, horizontale faible | Décentralisé            | Ajustement mutuel, standardisation des résultats et qualifications. |

## Analyse stratégique
- Basée sur le travail de Michel Crozier,
- Chaque acteur à ses propres objectifs, et sa stratégie personnelle,
- Un acteur est un homme au travail qui a un rôle actif.
- Un acteur participe à l'organisation,
- Un acteur interprète les consignes , ordres, ... (liberté).
### Rationalité limitée
L'employé est un acteur libre qui poursuit une stratégie pour ses objectifs personnels.

### Pouvoir
>[!Define] Pouvoir
>Capacité d'une personne à obtenir quelque chose de quelqu'un d'autre.

Porté par:
- l'expert,
- le marginal-sécant (permet la communication de plusieurs environnements),
- l'aiguilleur,
- le chef par statut.

### Règles du jeu
Beaucoup informelle.
Pour chaque acteur, quelles sont:
- ses buts,
- ses enjeux,
- ses sources de pouvoirs,
- ses contraintes,
- ses stratégies.

### Manager
- Distribue les tâches,
- Gère les congés, absences,
- Gestion des RH,
- Organise les réunions d'équipe,
- Interface les autres équipes et les clients,
- Gestion des conflits,
- Fixe les objectifs,
- Fait des rapports.

# Sujet pour l'évaluation
- Interaction avec mon tuteur de stage pour mon stage d'ING1.

>[!Define] Effet Hawthorne
>Quand on s'intéresse au travail des gens, ils travaillent mieux.