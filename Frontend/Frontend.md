# Langage utilisé
**Angular**
- Vérifier la version de node,
- Vérifier l'installation de npm,
- Installer angular: https://angular.io/cli.
**Firebase** en fin de module.
## ECMAScript
> [!Define] ECMAScript
>Norme logicielle de W3C qui dicte les normes de javascript pour assurer la rétro-compaptibilité.
## Contexte et binding en Javascript
Le moteur (eg V8) s'injecte dans un environnement. Quel que soit l'environnement, il va tourner, être léger, utiliser peu de ressources... Pour ce faire, il a besoin d'un moyen de dialoguer avec le système (eg nodeJS).

# Utilité des fichiers du projet
## tsconfig.json
Configure typescript (pour avoir du code plus robuste).
Normalement pas besoin d'y toucher (sauf si problèmes de versions).
## package.json
Dépendances du projet pour une application node.
Distingue les dépendances en développement et celles de l'hôte.
Elles sont installées dans **node_modules**.
## angular.json
Fichier de paramétrage de l'application.
## src/assets/
Fichier qui seront rajoutés dans l'appli (images, ...).
## src/index.html
N'est pas modifier dans 99% des cas.
## src/main.ts
Point d'entrée de l'application.
## src/app/
Dossier dans lequel on va travailler.
## src/app/app-module.js
Gestion des modules.
# Concepts importants
> [!Define] Modules
>Liste de classes référencées et réparties dans l'application.
>= librairie

>[!Define] Component  
>Sert à afficher une page ou un morceau de page.

# Utilisation
## Mise à jour de la page
Angular modifie la page automatiquement quand le code est modifié.
## Utilisation des variables
Il est possible de récupérer les variables du ficher ts dans le fichier html associé en utilisant des "moustaches": **{{ }}**
## Typage
```ts
nom: string = "klein";
listeMoustache: Array<string> = [];
```
## Méthodes
```ts
nom() {
}
```
## Constructeur
```ts
constructor() {
}
```
## Créer un composant
```shell
ng generate component template/footer
# ng g c template/footer
```
## Créer un modèle
```ts
ng g interface shared/models/usersI
```

# Services
```ts
ng g service shared/services/auth
```

>[!Define] Service  
>Sert à partager du code (injectable).
# Technologies multi-threads et mono-threads
>[!Example]  
PHP alloue un thread à chaque utilisateur.
Javascript ne fait qu'une seule opération à la fois.

>[!Failure] Inconvénient  
>Délai sur la gestion des événements (clics, ...)

D'où l'importance des requêtes XHR asynchrones.

>[!Define] XHR  
>**XML HTTP Request**: Requête depuis le navigateur vers une serveur distant (seul solution pour récupérer des données avec javascript).

Visibles dans Firefox -> Sidepannel -> Réseau -> Fetch/XHR

Angular ré-utilise la gestion de javascript de base et rajoute de la sécurité.

```ts
this.http.get<Array<EvenementI>>('assets/data/evenements.json').subscribe();
```

# Pipes personnalisés
```shell
ng g pipe shared/pipe/events
```
# Niveau 1 de sécutité
Lié à l'organisation de l'application.

>[!Hint]  
>Angular prend en charge la minification.
>``` bash
># Crée les fichiers qui seront mis à disposition des utilisateurs
>ng build
>```

Tout le code écrit est potentiellement accessible (car javascript).
*Solution*: Créer des modules.
```shell
ng g module organisation --routing
```

# Paramètres dans la route
Dans le routage "page/:parametre".

# Restreindre l'accès à un module
```shell
ng g guard shared/securite/auth
```
# Token
>[!Define] Token  
>Suite de caractères permettant de récupérer tous les éléments d'une session.

Trois éléments:
- **Header:** Ce que c'est et comment c'est,
- **Payload:** Données,
- **Verify Signature:** Vérification de l'encodage à l'aide d'une phrase secrète.

>[!Info]  
>[Visualisation](https://jwt.io/) (fournit aussi des librairies pour plusieurs languages)

## Procédé
1. L'utilisateur envoie une requête XHR avec id et password,
2. Le serveur utilise le token pour vérifier,
3. Le serveur envoie un code 200 (OK) à l'utlisateur.

Chaque requête doit inclure le token dans le header.
```
Authorization: Bearer my_token
```

## Interceptor
```shell
ng g interceptor shared/securite/token
```

# Librairies utiles
- material
- primeNG
