# Token
>[!Define] Token
>Suite de caractères permettant de récupérer tous les éléments d'une session.

Trois éléments:
- **Header:** Ce que c'est et comment c'est,
- **Payload:** Données,
- **Verify Signature:** Vérification de l'encodage à l'aide d'une phrase secrète.

>[!info]
>[Visualisation](https://jwt.io/) (fournit aussi des librairies pour plusieurs languages)

## Procédé
1. L'utilisateur envoie une requête XHR avec id et password,
2. Le serveur utilise le token pour vérifier,
3. Le serveur envoie un code 200 (OK) à l'utlisateur.

Chaque requête doit inclure le token dans le header.
```
Authorization: Bearer my_token
```

## Interceptor
```shell
ng g interceptor shared/securite/token
```

# Librairies utiles
- material
- primeNG