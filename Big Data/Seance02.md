>[!Define] Parquet file
>Document qui enregistre les données aux formats colonnes (facilite la compression, le sharding et la sélection de variables).

# Plateforme HUPI
https://hupi.hupi.io//
## Processus
- Dans un Datacenter local,
- En temps réel, automatiquement mis à jour.
1. Récupération des données à différents formats: localiser ces données, API, Kafka,
2. Stockage des données: MongoDB ou HDFS,
3. Exploitation: Spark, Python, Julia,
4. Visualisation.

>[!Define] Business Intelligence
>Transform data into business insights.

# Logstash
>[!Define] Logstash
>Gestionnaire d'events et de logging open source et centralisé.

## Constituants
- input,
- filtre,
- output.

## Paramétrage
Fichier .conf
``` conf
input {
	File {
		path => "/chemin/vers/inlog.log"
	}
}
output {
	File {
		path => "/chemin/vers/outlog.log"
	}
}
```
Filtres avec Grok