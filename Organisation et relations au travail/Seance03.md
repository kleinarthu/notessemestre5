# Rendu
- Au plus tard mardi.

# Préparation du cours de design
- Lire un texte.
# Exercice d'inclusion (dessin partagé)

# Analyse transactionnelle
>[!Define]
>Façon de décoder ce qui se passe dans une relation.

- Développé par Éric Bern.
## États du moi
- Enfant: Trois types (adapté, soumis / rebel / libre, créateur),
- Adulte: Parle avec des faits,
- Parent: Deux types (normatif: on l'a intégré, il faut / nourricier: prend soin).

## Postions de vie
- Je ne me sens pas OK et je sens que l'autre est OK: je me sens soumis, j'ai envie de fuir,
- Je me sens OK et je sens que l'autre n'est pas OK: relation de domination, on écrase l'autre,
- Je ne sens pas OK et je sens que l'autre n'est pas OK: démission, on est dans l'impasse,
- Je me sens OK et je sens que l'autre est OK: on peux coopérer, aller de l'avant avec l'autre.

# Assertivité
>[!Define] Assertivité
>Exprimer son opinion tout en respectant celui de l'autre.

## Comportements relationnels
- Fuyant,
- Assertif,
- Manipulateur,
- Agressif.

# Signes de reconnaissances
- Fait partie des motivations intrinsèques,
- Rare en France (on commence par critiquer).

## Types
- Conditionnel: pour le contexte,
- Inconditionnel: pour la personne.

|         | Conditionnel                      | Indonditionnel    |
| ------- | --------------------------------- | ----------------- |
| Positif | Valoriser, féliciter              | Autonomie, estime |
| Négatif | Critiquer, inviter l'amélioration | Dévaloriser       |
## Formes
- Existentiel: Reconnaître l'humanité de quelqu'un,
- Pratique: Reconnaître les méthodes, les compétences de quelqu'un,
- Investissement: Reconnaître l'effort,
- Résultats: Reconnaître la productivité, la qualité des résultats de quelqu'un.

## Sources
- Donner à quelqu'un,
- Recevoir de quelqu'un,
- Refuser les signes négatifs inconditionnels,
- Demander à quelqu'un,
- S'en donner soi-même.

>[Application de partage de signes de reconnaissances](https://fr.listenleon.com/)

# Gestion des conflits
## Types
- Conflit cognitif: Pas d'accord sur une idée,
- Conflit relationnel sur les émotions: Implicite,
- Conflit relationnel sur le pouvoir: Implicite,

## Étapes
- Accumulation: petites frustrations,
- Indifférence contrôlée,
- Évitement,
- Guerre froide,
- Guerre ouverte.

## Relations au conflit
- Éviter,
- S'adapter,
- Négocier,
- Lutter,
- Collaborer.

## Interactions à risque
- Polémique,
- Passage en Force,
- Stratagème,
- Disqualification.