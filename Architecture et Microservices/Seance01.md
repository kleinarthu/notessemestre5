>[!Important] Objectifs
>- Apprendre à coder pour les IA,
>- Créer et utiliser des modèles pour les ICC.

# Brainstorming
Architecture et micro-services:
- Services autonomes,
- Qui peuvent passer à l'échelle à la demande,
- Découper les gros monolithes en plus petites applications,
- **Résilience**.

# Apprentissage des architectures
## Jusqu'en cycle préparatoire
- Faire ce que l'on veut à partir d'un language.
## En cycle ingé
- Modèle MVC.
## Aujourd'hui
- On sert tous les services,
- On résonne par domaine,
-  On transmet les requêtes à l'API Gateway qui transmet au bon service en répartissant la charge.
# Définitions

>[!Define] CDN
>Content delivery Network

>[!Define] API Gateway
>Système qui reçoit les requêtes et les distribue

>[!Define] Architecture et microservices
>Développer une application unique sous la fore d'une suite logicielle intégrant plusieurs services contruits autour de la capacité d'être adaptés de manière **indépendantes**.
# Histoire
- Terme né en 2011,
- Popularisé en 2014,
- Mis en valeur par Netflix.
# Caractéristiques d'un service
Faire une chose mais bien:
- Déploiement et test automatisés,
- Élastique (mise à l'échelle),
- Résilient,
- Composable,
- Minimal,
- Complet.

>[!Success] Avantages
>- Facilement **testable** (car code petit donc tests petits),
>- Faiblement couplé (juste au niveau du réseau),
>- Déployés **indépendamment**,
>- Permet de moduler le budget, être plus flexible dans la répartition des employés,
>- Travailler par plus petites équipes.
# Communication entre services
>[!Define] API
> Interface de programmation applicative

Laisser les back-ends exposer leurs fonctionnalités via des API.
## REST
>[!Define] REST
>**Representational State Transfer**: Style d'architecture logicielle définissant un ensemble de contrainte à utiliser pour créer des services web.

>[!Define] CWA
>**Custom web app**: utiliser la puissance des navigateurs pour distribuer plus facilement les applications.

>[!Example] API Chuck Norris
>https://api.chucknorris.io/jokes/random
>``` shell
>curl -XGET https://api.chucknorris.io/jokes/random && echo ""
>```

>[!Info] Postman
>[Download Postman](https://www.postman.com/downloads/)

# Qu'est ce qu'une requête HTTP
>[!Define] HTTP
>Hyper Text Transfer Protocol

HTTP: Couche 7
HTTPS: Couche 6

= Grosse chaîne de caractères formatée
## Méthode HTTP
### GET
- Méthode par défaut,
- **Récupérer** une information.
### HEAD
- Lire uniquement les **en-têtes**,
- Valider l'URL.
### POST
- Envoyer des données aux serveurs pour les traiter immédiatement,
- Surtout pour le **processing**.
### PUT
- Envoyer de la données pour du **stockage**.
- Modifications destructives.
### DELETE
- **Supprimer** une donnée du système.
### PATCH
- Comme PUT mais uniquement pour **remplacer**.

>[!Warning] Attention
>Certaines personnes ne respectent pas ces conventions.
## URL
>[!Define] URL (= URI)
>Chemin vers une ressource dans un serveur donné.

>[!Question] Comment construire une URL?
>>[!Example]
>>/v1/browse/categories/{category_id}: un élément
>>/v1/browse/categories: liste des éléments
## En-têtes
En-têtes au format **NOM: VALEUR**
>[!Info] List des en-têtes
>[Site de mozilla](https://developer.mozilla.org/fr/docs/Web/HTTP/Headers)
>[MIME Type communs](https://developer.mozilla.org/fr/docs/Web/HTTP/Basics_of_HTTP/MIME_types/Common_types)
## Corps
Fournir la donnée structurée (prioriser **json**).
## Codes de retour
3 chiffres:
- 1xx: Information,
- 2xx: Succès,
- 3xx: Redirection,
- 4xx: Erreur client,
- 5xx: Erreur serveur.
### 1xx
- 100 -> Continue: Attente de la suite de la requête (pour éviter les requêtes trop longues et diviser les payloads),
- 102 -> Processing: Traitement de la requête en cours,
- 103 -> Early Hints: Retour petit à petit de la donnée demandée.
### 2xx
- 200 -> 0K: Requête traitée avec succès,
- 201 -> Created: Succès de la création de document.
- 202 -> Acceptée mais pas encore traitée.
### 3xx
- 300 -> Multiple choice: l'URL pointe vers plusieurs ressources,
- 301 -> Moved permanently,
- 302 -> Move temporarlly.
### 4xx
- 400 -> Bad request: Mauvais format,
- 401 -> Unauthorized: Besoin d'authentification,
- 403 -> Forbidden: Pas le droit avec cette identité,
- 404 -> Not Found
- 418 -> I'm a teapot
### 5xx
- 500 -> Internal Server Error,
- 501 -> Not implemented,
- 502 -> Bad gateway,
- 503 -> Service unavailable: Maintenance,
- 504 -> Gateway Time-Out.

>[!Important] Bonnes pratiques pour le JSON
>- Inclure la raison du code de retour,
>- Renvoyer la donnée qui vient d'être mise à jour.
# Stack Technique
Ne pas faire de "trucs folkloriques" => utiliser les technos vraiment utilisées.
>[!Info]
>[Classement des langages les plus utilisés dans les entreprises](https://www.tiobe.com/tiobe-index/)
## Backend
### Python
- django,
- Flask,
- **FastAPI**.
### Java
- spring,
- Quarkus,
- Micronaut.
### C\#
- ASP.NET.
### JavaScript
- Express,
- nest,
- sails,
### PHP
- Laravel,
- Codeigniter,
- Symfony.
## Frontend
- ReactJS,
- **Angular**,
- Vue,
- ...

# Python: Introduction
## Caractéristiques
- Multiparadigme,
- Interprété,
- Surcouche à C,
- Multiplateforme,
- Typé dynamiquement,
- Fortement typé.

## Exécution
```python
from app import app

if __name__ == '__main__':
	app()
```
## Assignation
```python
var = 12
x = "toto"
y = None
```
## Types
- int
- float,
- str,
- complex
- boolean
## Affichage
```python
print(x)
```
## Condition
```python
if var > 10:
	pass
elif condition:
	pass
else:
	pass
```
## Ternaire
```python
a = 1 if b else 0
```
## Comparaisons
- <,
- \>,
- <=,
- \>=,
- \=\=,
- is (teste aussi le typage),
- !=,
- and,
- or,
- not,
- is not.
## N-uplet
Immutable
```python
obj = "toto", "tata", "tutu"
obj[0]
```
## Listes
Les listes peuvent contenir des données de types différents.
``` python
colors = ["red", "green", "blue", "purple"]
colors.append("brown")
colors.remove("red")
deleted_item = colors.pop()
colors.insert(len(colors), "brown")
blue = colors[1]
del colors[1]
colors[-1] # Dernier élément
colors[1: 3] # 3 exclu
```

## Dictionnaire
```python
dico = {'key': 'value'}
dico['new_key'] = 'new_value'
```
## Boucle
```python
while condition:
	pass
```
## For
```python
for char in "hello":
	print(char)
	# break pour quitter
	# continue pour passer à l'étape suivante
for i in range(5):
	print(i)
for i, color in enumerate(colors):
	print(f"colors[{i}] = {color}")
for color, ratio in zip(colors, ratios):
	pass
for key in dico: # ou dico.keys()
	print(dico[key])
for key, value in dico.items():
	pass
for value in dico.values():
	pass
```
## Compréhension
```python
dico = {key: value for key, value in zip(categories, objects) if value > 5}
```
## Import
```python
import monModule
from monModule import ceDontJAiBesoin
```
## Fonction
```python
from typing import Any, List

def sum_list(l: List[Any], type_ = int) -> Any:
	""" Sum of the elements in list from a given type

	Args:
		list (List[Any]): The list to apply sum
		type_ ([type], {optional}): The type to give to identify what to sum. Defaults to int.

	Returns:
		Any: Thre result of the sum.
  """
	res = 0
	for e in l:
		if isinstance(e, type_):
			res += e
	return res
```